
/**
 * REST Controller 
 * Gestisce le request per la classe Customer
 * @author LeoGrasso
 */

package it.leonardograsso.epicenergyservices.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.leonardograsso.epicenergyservices.models.customer.Customer;
import it.leonardograsso.epicenergyservices.services.impl.CustomerServiceImpl;

@RestController
@RequestMapping("/customers")
public class CustomerController {
	
	@Autowired
	private CustomerServiceImpl customerServ;
	
	/*
	 * Classe Message di tipo Utility
	 * Raccoglie i messaggi di errore delle eccezioni
	 * Il costruttore privato mi assicura che questa classe non possa essere istanziata
	 */
	protected static class Messages {
		public static final String ERROR_SAVE = "An unexpected error occurred while saving";
		public static final String ERROR_UPDATE = "An unexpected error occurred while updating";
		public static final String ERROR_DELETE = "An unexpected error occurred while deleting";
		public static final String ERROR_LIST = "An error occurred while creating the customer list";
		public static final String NOT_COMPANY_NAME = "Company name not found";
		
		

		  private Messages() {
			    throw new IllegalStateException("Utility class");
			  }
	}
	
	static Logger log = LoggerFactory.getLogger(CustomerController.class);
	
	
	/**
	 * Gestisce richiesta POST inserimento nuovo cliente
	 * @param customer	riceve un oggetto Customer in formato Json
	 * @return			un oggetto Customer in formato Json
	 */
	@PostMapping(value = "/add")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<Customer> addCompany(@RequestBody Customer customer){
		try {
			return new ResponseEntity<>(customerServ.saveCustomer(customer), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_SAVE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	/**
	 * Gestisce richiesta POST modifica cliente esistente nel database
	 * @param customer	riceve un oggetto Customer in formato Json
	 * @param id		id cliente da modificare
	 * @return			l' oggetto Customer modificato in formato Json
	 */
	@PostMapping(value = "/update/{id}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<Customer> updateCompany(@PathVariable int id, @RequestBody Customer customer){
		try {
			return new ResponseEntity<>(customerServ.updateCustomer(id, customer), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_UPDATE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	/**
	 * Gestisce richiesta GET cancellazione cliente esistente nel database
	 * @param id	is cliente da cancellare
	 * @return		l' oggetto Customer cancellato in formato Json
	 */
	@GetMapping(value = "/delete/{id}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<Customer> deleteCompany(@PathVariable int id){
		try {
			return new ResponseEntity<>(customerServ.deleteCustomer(id), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_DELETE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	/**
	 * Gestisce richiesta GET per ottenere un oggetto Customer a partire dal nome
	 * @param companyname	nome dell'azienda da cercare
	 * @return				l' oggetto Customer con il nome ricercato
	 */
	@GetMapping(value = "/getbyname")
	@PreAuthorize("hasRole('admin') or hasRole('user')")
	public ResponseEntity<Customer> getByName(@RequestParam String companyName){
		try {
			return new ResponseEntity<>(customerServ.findByCompanyNameIgnoreCase(companyName), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.NOT_COMPANY_NAME, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	/**
	 * Gestisce richiesta GET per ottenere la lista di aziende per tipo di fatturato
	 * 
	 * @param revenue	questo parametro può avere i seguenti valori: 
	 * 					"MICRO_IMP_MAX2ML" 		--> micro impresa;
	 * 					"PICCOLA_IMP_MAX10ML"	--> piccola impresa; 
	 * 					"MEDIA_IMP_MAX50ML"		--> media impresa;
	 * 					"GRANDE_IMP_OVER50ML"	--> grande impresa;
	 * 
	 * @param page		numero di pagine 
	 * 
	 * @param size		numero di elementi per pagina
	 * 
	 * @param sort		ordinamento che può avere i seguenti valori: 
	 * 					"companyName"		--> nome azienda
	 * 					"revenue"			-->	fatturato
	 * 					"creationDate"		--> data creazione nel database
	 * 					"lastContactDate"	--> data ultimo contatto
	 * 					"corporateForm"		--> forma societaria
	 * 					"province"          --> acronimo provincia sede legale
	 * 
	 * @return			lista di clienti con paginazione ed ordinamento personalizzati
	 */
	@GetMapping(value = "/get/revenue")
	@PreAuthorize("hasRole('admin') or hasRole('user')")
	public ResponseEntity<List<Customer>> getAllByRevenue(@RequestParam String revenue, @RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "1") Integer size, @RequestParam(defaultValue = "id") String sort){
		
		try {
			List<Customer> customers = customerServ.findAllByRevenue(revenue, page, size, sort);
			return new ResponseEntity<>(customers, HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_LIST, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	/**
	 * Gestisce richiesta GET per ottenere la lista di aziende per data di creazione
	 * 
	 * @param day		giorno
	 * @param month		mese
	 * @param year		anno
	 * 
	 * @param page		numero di pagine 
	 * 
	 * @param size		numero di elementi per pagina
	 * 
	 * @param sort		ordinamento che può avere i seguenti valori: 
	 * 					"company_name"		--> nome azienda
	 * 					"revenue"			-->	fatturato
	 * 					"creation_date"		--> data creazione nel database
	 * 					"last_contact_date"	--> data ultimo contatto
	 * 					"corporate_form"	--> forma societaria
	 * 					"province"          --> acronimo provincia sede legale
	 * 
	 * @return			lista di clienti con paginazione ed ordinamento personalizzati
	 */
	@GetMapping(value = "/get/creationdate")
	@PreAuthorize("hasRole('admin') or hasRole('user')")
	public ResponseEntity<List<Customer>> getAllByCreationDate
		(@RequestParam int day, @RequestParam int month, @RequestParam int year, @RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "1") Integer size, @RequestParam(defaultValue = "id") String sort){
		
		try {
			List<Customer> customers = customerServ.findAllByCreationDate(day, month, year, page, size, sort);
			return new ResponseEntity<>(customers, HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_LIST, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	/**
	 * Gestisce richiesta GET per ottenere la lista di aziende per data di ultimo contatto
	 * 
	 * @param day		giorno
	 * @param month		mese
	 * @param year		anno
	 * 
	 * @param page		numero di pagine 
	 * 
	 * @param size		numero di elementi per pagina
	 * 
	 * @param sort		ordinamento che può avere i seguenti valori: 
	 * 					"company_name"		--> nome azienda
	 * 					"revenue"			-->	fatturato
	 * 					"creation_date"		--> data creazione nel database
	 * 					"last_contact_date"	--> data ultimo contatto
	 * 					"corporate_form"	--> forma societaria
	 * 					"province"          --> acronimo provincia sede legale
	 * 
	 * @return			lista di clienti con paginazione ed ordinamento personalizzati
	 */
	@GetMapping(value = "/get/lastcontactdate")
	@PreAuthorize("hasRole('admin') or hasRole('user')")
	public ResponseEntity<List<Customer>> getAllByLastContactDate
		(@RequestParam int day, @RequestParam int month, @RequestParam int year, @RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "1") Integer size, @RequestParam(defaultValue = "id") String sort){
		
		try {
			List<Customer> customers = customerServ.findAllByLastContactDate(day, month, year, page, size, sort);
			return new ResponseEntity<>(customers, HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_LIST, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	
	
	
	
	

}
