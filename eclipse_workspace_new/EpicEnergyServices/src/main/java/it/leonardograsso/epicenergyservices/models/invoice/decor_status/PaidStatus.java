
/**
 * Classe che rappresenta lo stato della fattura pagata
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.models.invoice.decor_status;

import org.springframework.stereotype.Component;

import it.leonardograsso.epicenergyservices.models.invoice.InvoiceStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Component
public class PaidStatus extends InvoiceStatus{
	
	
	/**
	 * setta lo stato in "pagata"
	 * e lo restituisce
	 */
	@Override
	public String getStatusName() {
		setStatusName("pagata");
		return super.getStatusName();
	}


	
	


}
