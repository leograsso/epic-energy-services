
/**
 * Repository per indirizzi
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.repositories;

import java.util.List;
import java.util.Optional;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.leonardograsso.epicenergyservices.models.customer.Address;


@Repository
public interface AddressRepository extends JpaRepository<Address, Integer>{
	
	/**
	 * cerca tipi di indirizzo per cliente
	 * 
	 * @param customerId	id cliente
	 * @return				lista di tipi di indirizzo per cliente
	 */
	List<Optional<Address>> findTypeByCustomerId(int customerId);

}
