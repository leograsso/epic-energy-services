
/**
 * Repository per user
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.leonardograsso.epicenergyservices.models.user.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	
	/**
	 * Ricerca tramite email.
	 */
	Optional<User> findByEmail(String email);
}


