
/**
 * Gestione di utenti.
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.leonardograsso.epicenergyservices.models.user.Role;
import it.leonardograsso.epicenergyservices.models.user.User;


public interface UsersService extends BaseService {
	/**
	 * Creazione di utente.
	 */
	User register(User user, int role);

	/**
	 * Recupera un utente se esiste.
	 * 
	 * @param email l'email dell'utente.
	 */
	Optional<User> exists(String email);

	/**
	 * Recupera un utente tramite email.
	 * 
	 * @param email l'email dell'utente.
	 */
	User getByEmail(String email);

	/**
	 * Recupera tutti gli utenti.
	 */
	Page<User> getAll(Pageable pageable);

	/**
	 * Aggiorna un utente.
	 * 
	 * @param email l'email dell'utente.
	 * @param data  le informazioni per la modifica.
	 */
	User update(String email, User data);

	/**
	 * Elimina un utente.
	 * 
	 * @param email l'email dell'utente.
	 */
	User delete(String email);

	/**
	 * Aggiunge un utente ad un ruolo.
	 * 
	 * <pre>
	 * Se il ruolo non esiste, lo aggiunge.
	 * </pre>
	 * 
	 * @param email l'email dell'utente.
	 * @param role  il ruolo.
	 * 
	 */
	User addToRole(String email, String role);

	/**
	 * Rimuove un utente da un ruolo.
	 * 
	 * @param email l'email dell'utente.
	 * @param role  il ruolo.
	 */
	User removeFromRole(String email, String role);

	/**
	 * Recupera tutti i ruoli.
	 */
	List<String> getRoles();

	/**
	 * Aggiunge un ruolo.
	 * 
	 * @param role il nome del ruolo.
	 */
	Role addRole(String role);

	/**
	 * Elimina un ruolo.
	 * 
	 * @param role il nome del ruolo.
	 * @return
	 */
	Role deleteRole(String role);
}
