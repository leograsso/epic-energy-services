
/**
 * Classe che rappresenta lo stato della fattura generico
 * viene estesa dalle classi che definiscono uno stato
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.models.invoice;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.leonardograsso.epicenergyservices.models.BaseEntity;
import it.leonardograsso.epicenergyservices.models.customer.CompanyDataItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity (name = "invoices_status")
public class InvoiceStatus extends BaseEntity implements CompanyDataItem{
	
	/**
	 * set di fatture a cui è riferito lo stato
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
	@JsonIgnore
	private Set<Invoice> invoices = new HashSet<>();
	
	/**
	 * valorizzazione dello stato della fattura
	 */
	@Column(unique = true)
	private String statusName;
	

}
