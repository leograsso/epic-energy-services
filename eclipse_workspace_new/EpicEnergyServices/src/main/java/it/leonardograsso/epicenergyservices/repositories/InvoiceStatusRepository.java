
/**
 * Repository dello stato delle fatture
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.leonardograsso.epicenergyservices.models.invoice.InvoiceStatus;


@Repository
public interface InvoiceStatusRepository extends JpaRepository <InvoiceStatus, Integer> {
	
	/**
	 * 
	 * @param id	id stato fattura
	 * @return		denominazione dello stato fattura
	 */
	Optional<String> findStatusNameById(int id);
	
	/**
	 * 
	 * @param statusName	nome dello stato fattura
	 * @return				un oggetto InvoiceStatus
	 * 
	 * Non ritorna una lista perchè il nome dello stato è univoco
	 */
	Optional<InvoiceStatus> findInvoiceStatusByStatusName(String statusName);

	/**
	 * 
	 * @param statusName	denominazione dello stato
	 * @return				un oggetto InvoiceStatus
	 */
	Optional<InvoiceStatus> findInvoiceStatusByStatusNameIgnoreCase(String statusName);
	
	

}
