
/**
 * Business Service dei numeri di telefono
 * 
 * @author LeoGrasso
 * 
 */

package it.leonardograsso.epicenergyservices.services.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.leonardograsso.epicenergyservices.models.customer.PhoneNumber;
import it.leonardograsso.epicenergyservices.repositories.PhoneNumberRepository;
import it.leonardograsso.epicenergyservices.services.PhoneNumberService;
import it.leonardograsso.epicenergyservices.services.exceptions.EntityNotFoundException;
import it.leonardograsso.epicenergyservices.services.exceptions.ServiceException;

@Service
public class PhoneNumberServiceImpl implements PhoneNumberService{
	
	Logger log = LoggerFactory.getLogger(PhoneNumberServiceImpl.class);
	
	@Autowired
	private PhoneNumberRepository pr;
	

	@Override
	public PhoneNumber savePhoneNumber(PhoneNumber phoneNumber) {
		try {
			return pr.save(phoneNumber);
		} catch (Exception e) {
			log.error("Unable to save the phoneNumber {}", phoneNumber.getNumber());
			throw new ServiceException(e);
		}
	}

	
	@Override
	public PhoneNumber updatePhoneNumber(int id, PhoneNumber phoneNumber) {
		try {
			var p = pr.findById(id).orElseThrow(() -> new EntityNotFoundException(id, PhoneNumber.class));
			p.setCustomer(phoneNumber.getCustomer());
			p.setReferenceContact(phoneNumber.getReferenceContact());
			p.setNumber(phoneNumber.getNumber());
			p.setType(phoneNumber.getType());
			return pr.save(p);
		} catch (EntityNotFoundException e) {
			if (e.getType().equals(PhoneNumber.class))
				log.error("Unable to find the phoneNumber : {}", phoneNumber.getNumber());
				throw e;
		} catch (Exception e) {
			log.error("Unable to update the phoneNumber : {}", phoneNumber.getNumber());
			throw new ServiceException(e);
		}
	}

	
	@Override
	public PhoneNumber delitePhoneNumber(int id) {
		try {
			PhoneNumber p = pr.findById(id).orElseThrow(() -> new EntityNotFoundException(id, PhoneNumber.class));
			pr.delete(p);
			return p;
		} catch (EntityNotFoundException e) {
			log.error("Unable to find the phoneNumber : {}", id);
			throw e;
		} catch (Exception e) {
			log.error("Unable to delete phoneNumber whith id {}", id);
			throw new ServiceException(e);
		}
	}


	@Override
	public Optional<PhoneNumber> findById(int id) {
		try {
			return pr.findById(id);
		} catch (Exception e) {
			log.error("Unable to find phone number whith id: {}", id);
			throw new EntityNotFoundException(id, PhoneNumber.class);
		}
	}

}
