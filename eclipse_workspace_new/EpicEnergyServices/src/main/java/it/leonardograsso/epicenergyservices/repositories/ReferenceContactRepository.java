package it.leonardograsso.epicenergyservices.repositories;

/**
 * Repository per referenti aziendali
 * 
 * @author LeoGrasso
 *
 */

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.leonardograsso.epicenergyservices.models.customer.ReferenceContact;

@Repository
public interface ReferenceContactRepository extends JpaRepository<ReferenceContact, Integer> {

}
