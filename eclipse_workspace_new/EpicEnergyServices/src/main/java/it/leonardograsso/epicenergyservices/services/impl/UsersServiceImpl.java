package it.leonardograsso.epicenergyservices.services.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.leonardograsso.epicenergyservices.models.user.Role;
import it.leonardograsso.epicenergyservices.models.user.User;
import it.leonardograsso.epicenergyservices.repositories.RoleRepository;
import it.leonardograsso.epicenergyservices.repositories.UserRepository;
import it.leonardograsso.epicenergyservices.services.UsersService;
import it.leonardograsso.epicenergyservices.services.exceptions.EntityNotFoundException;
import it.leonardograsso.epicenergyservices.services.exceptions.ServiceException;

@Service
public class UsersServiceImpl implements UsersService {

	Logger log = LoggerFactory.getLogger(UsersServiceImpl.class);
	
	@Autowired
	private UserRepository users;

	@Autowired
	private RoleRepository roles;

	@Autowired
	private PasswordEncoder encoder;

	/***
	 * assegna il ruolo e salva un nuovo utente
	 */
	@Override
	public User register(User user, int role) {
		try {
			User newUser = new User();
			Role roleAdmin = new Role().builder().withName("admin").build();
			Role roleUser = new Role().builder().withName("user").build();
			if (role == 1) {
				newUser.getRoles().add(roleAdmin);
			} else if (role == 2) {
				newUser.getRoles().add(roleUser);
			} else if (role == 3) {
				newUser.getRoles().add(roleUser);
				newUser.getRoles().add(roleAdmin);
			}
			newUser.setName(user.getName());
			newUser.setSurname(user.getSurname());
			newUser.setEmail(user.getEmail());
			newUser.setPassword(encoder.encode(user.getPassword()));
			newUser.setActive(true);
			users.save(newUser);
			return newUser;
		} catch (Exception e) {
			log.error("Exception when create user {}", user);
			throw new ServiceException(e);
		}
	}

	@Override
	public User getByEmail(String email) {
		try {
			return exists(email).orElseThrow(() -> new EntityNotFoundException(email, User.class));
		} catch (EntityNotFoundException e) {
			log.error("Unable to retrieve user by email {}", email);
			throw e;
		} catch (Exception e) {
			log.error("Exception when retrieve user by email {}", email);
			throw new ServiceException(e);
		}
	}

	@Override
	public Page<User> getAll(Pageable pageable) {
		try {
			return users.findAll(pageable);
		} catch (Exception e) {
			log.error("Exception when retrieve users");
			throw new ServiceException(e);
		}
	}

	@Override
	public User update(String email, User data) {
		try {
			var user = users.findByEmail(email).orElseThrow(() -> new EntityNotFoundException(email, User.class));
			user.setName(data.getName());
			user.setPassword(data.getPassword());
			return users.save(user);
		} catch (EntityNotFoundException e) {
			log.error("Unable to find user({})", email);
			throw e;
		} catch (Exception e) {
			log.error("Unable to update user({}) with data {}", email, data);
			throw new ServiceException(e);
		}
	}

	@Override
	public User delete(String email) {
		try {
			var old = users.findByEmail(email).orElseThrow(() -> new EntityNotFoundException(email, User.class));
			users.delete(old);
			return old;
		} catch (EntityNotFoundException e) {
			log.error("Unable to find user({})", email);
			throw e;
		} catch (Exception e) {
			log.error("Unable to delete user({})", email);
			throw new ServiceException(e);
		}
	}

	@Override
	public Optional<User> exists(String email) {
		try {
			return users.findByEmail(email);
		} catch (Exception e) {
			log.error("Exception when find user by email {}", email);
			throw new ServiceException(e);
		}
	}

	public boolean isInRole(String email, String role) {
		try {
			var user = getByEmail(email);
			return user.getRoles().stream().anyMatch(r -> r.getName().equals(role));
		} catch (EntityNotFoundException e) {
			log.error("Unable to find user {}", email);
			throw e;
		} catch (Exception e) {
			log.error("Exception when test if user {} is in role {}", email, role);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public User addToRole(String email, String role) {
		try {
			if (isInRole(email, role))
				return getByEmail(email);

			var user = getByEmail(email);
			var r = roles.findByName(role).orElse(addRole(role));
			user.getRoles().add(r);
			return users.save(user);
		} catch (EntityNotFoundException e) {
			log.error("Unable to find user {}", email);
			throw e;
		} catch (Exception e) {
			log.error("Exception when add user {} to role {}", email, role);
			throw new ServiceException(e);
		}
	}

	@Override
	public User removeFromRole(String email, String role) {
		try {
			var user = getByEmail(email);
			user.getRoles().removeIf(r -> r.getName().equals(role));
			return users.save(user);
		} catch (Exception e) {
			log.error("Exception when remove user {} from role {}", email, role);
			throw new ServiceException(e);
		}
	}

	@Override
	public List<String> getRoles() {
		try {
			return roles.findAll().stream().map(Role::getName).sorted().collect(Collectors.toList());
		} catch (Exception e) {
			log.error("Exception when retrieve roles", e);
			throw new ServiceException(e);
		}
	}

	@Override
	public Role addRole(String role) {
		try {
			return roles.save(Role.builder().withName(role).build());
		} catch (Exception e) {
			log.error("Unable to add role {}", role);
			throw new ServiceException(e);
		}
	}

	@Override
	public Role deleteRole(String role) {
		try {
			var r = roles.findByName(role).orElseThrow(() -> new EntityNotFoundException(role, Role.class));
			roles.delete(r);
			return r;
		} catch (EntityNotFoundException e) {
			log.error("Unable to find role {}", role);
			throw e;
		} catch (Exception e) {
			log.error("Unable to add role {}", role);
			throw new ServiceException(e);
		}
	}
}
