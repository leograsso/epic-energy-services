
/**
 * Classe che rappresenta lo stato della fattura non pagata
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.models.invoice.decor_status;


import org.springframework.stereotype.Component;

import it.leonardograsso.epicenergyservices.models.invoice.InvoiceStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Component
public class UnsolvedStatus extends InvoiceStatus{
	
	
	/**
	 * setta lo stato in "insoluta"
	 * e lo restituisce
	 */
	@Override
	public String getStatusName() {
		setStatusName("insoluta");
		return super.getStatusName();
	}

}
