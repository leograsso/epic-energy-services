
/**
 * UserDetailsServiceImpl 
 * Implementa l’interfaccia UserDetailsService fornita dal modulo Spring Security.
 * L'interfaccia UserDetailsService viene utilizzata per recuperare i dati relativi all'utente. 
 * Ha un metodo chiamato loadUserByUsername() che può essere sovrascritto per personalizzare il processo di ricerca dell'utente
 * UserDetailsService restituisce un'implementazione UserDetails che contiene le GrantedAuthorities
 * 
 * @author LeoGrasso
*/

package it.leonardograsso.epicenergyservices.security.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.leonardograsso.epicenergyservices.models.user.User;
import it.leonardograsso.epicenergyservices.repositories.UserRepository;



@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UserRepository userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> user = userRepository.findByEmail(username);
		if (user.isPresent()) {
			return UserDetailsImpl.build(user.get());
		} else {
			throw new UsernameNotFoundException("Utente " + username + "non trovato!");
		}
	}

}
