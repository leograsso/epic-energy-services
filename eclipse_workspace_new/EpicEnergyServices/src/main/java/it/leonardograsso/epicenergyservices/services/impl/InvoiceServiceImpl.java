
/**
 * Service della classe Invoice
 * 
 * @author LeoGrasso
 * 
 */

package it.leonardograsso.epicenergyservices.services.impl;



import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import it.leonardograsso.epicenergyservices.models.invoice.Invoice;
import it.leonardograsso.epicenergyservices.repositories.InvoiceRepository;
import it.leonardograsso.epicenergyservices.services.InvoiceService;
import it.leonardograsso.epicenergyservices.services.exceptions.EntityNotFoundException;
import it.leonardograsso.epicenergyservices.services.exceptions.ServiceException;

@Service
public class InvoiceServiceImpl implements InvoiceService {
	
	Logger log = LoggerFactory.getLogger(InvoiceServiceImpl.class);
	
	@Autowired
	private InvoiceRepository invRep;
	
	@Autowired
	private InvoiceStatusServiceImpl invoiceStatus;
	
	@Autowired
	private CustomerServiceImpl customerServ;

	
	@Override
	public Invoice saveInvoice(int customerId, Invoice invoice) {
		try {
			var customer = customerServ.findById(customerId).get();
			//assegna cliente alla fattura
			invoice.setCustomer(customer);
			//aggiorna data ultimo contatto dell'azienda cliente
			customer.setLastContactDate(LocalDate.now());
			//id = 2 corrisponde allo stato fattura insoluto
			invoice.setStatus(invoiceStatus.getById(2));
			//assegna la data del momento della creazione
			LocalDate creationDate = LocalDate.now();
			invoice.setDate(creationDate);
			//assegna l'anno
			invoice.setYear(creationDate.getYear());
			return invRep.save(invoice);
		} catch (Exception e) {
			log.error("Unable to save invoice N {}", invoice.getNumber());
			throw new ServiceException(e);
		} 
	}
	

	@Override
	public Invoice updateInvoice(int id, Invoice invoice) {
		try {
			//recupera il cliente
			var customer = invRep.findById(id).get().getCustomer();
			//aggiorna data ultimo contatto dell'azienda cliente
			customer.setLastContactDate(LocalDate.now());
			var inv = invRep.findById(id).orElseThrow(() -> new EntityNotFoundException(id, Invoice.class));
			inv.setYear(invoice.getYear());
			inv.setDate(invoice.getDate());
			inv.setAmountDue(invoice.getAmountDue());
			inv.setNumber(invoice.getNumber());
			inv.setStatus(invoice.getStatus());
			return invRep.save(inv);
		} catch (EntityNotFoundException e) {
			if (e.getType().equals(Invoice.class))
				log.error("Unable to find invoice N {}", invoice.getNumber());
				throw e;
		} catch (Exception e) {
			log.error("Unable to update invoice N {}", invoice.getNumber());
			throw new ServiceException(e);
		}
	}

	
	@Override
	public Invoice deleteInvoice(int id) {
		try {
			//recupera il cliente
			var customer = invRep.findById(id).get().getCustomer();
			//aggiorna data ultimo contatto dell'azienda cliente
			customer.setLastContactDate(LocalDate.now());
			Invoice inv = invRep.findById(id).orElseThrow(() -> new EntityNotFoundException(id, Invoice.class));
			invRep.delete(inv);
			return inv;
		} catch (EntityNotFoundException e) {
			log.error("Unable to find invoice with {}", id);
			throw e;
		} catch (Exception e) {
			log.error("Unable to delete invoice with {}", id);
			throw new ServiceException(e);
		}
	}

	
	@Override
	public List<Invoice> findAllByCustomerId(int customerId, Integer page, Integer size, String sort) {
		try {
			Pageable pageable = PageRequest.of(page, size, Sort.by(sort));
			Page<Invoice> pagedResult = invRep.findAllByCustomerId(customerId, pageable);
			if(pagedResult.hasContent()) {
				return pagedResult.getContent();
			} else {
				return new ArrayList<Invoice>();
	    	}
		} catch (Exception e) {
			log.error("Unable to find invoices with customer Id: {}", customerId);
			throw new ServiceException(e);
		}
	}

	
	@Override
	public List<Invoice> findAllByStatusId(int statusId, Integer page, Integer size, String sort) {
		try {
			Pageable pageable = PageRequest.of(page, size, Sort.by(sort));
			Page<Invoice> pagedResult = invRep.findAllByStatusId(statusId, pageable);
			if(pagedResult.hasContent()) {
				return pagedResult.getContent();
			} else {
				return new ArrayList<Invoice>();
	    	}
		} catch (Exception e) {
			log.error("Unable to find invoices with status Id: {}", statusId);
			throw new ServiceException(e);
		}
	}

	
	@Override
	public List<Invoice> findAllByDate(LocalDate date, Integer page, Integer size, String sort) {
		try {
			Pageable pageable = PageRequest.of(page, size, Sort.by(sort));
			Page<Invoice> pagedResult = invRep.findAllByDate(date, pageable);
			if(pagedResult.hasContent()) {
				return pagedResult.getContent();
			} else {
				return new ArrayList<Invoice>();
	    	}
		} catch (Exception e) {
			log.error("Unable to find invoices with date: {}", date);
			throw new ServiceException(e);
		}
	}

	
	@Override
	public List<Invoice> findAllByYear(int year, Integer page, Integer size, String sort) {
		try {
			Pageable pageable = PageRequest.of(page, size, Sort.by(sort));
			Page<Invoice> pagedResult = invRep.findAllByYear(year, pageable);
			if(pagedResult.hasContent()) {
				return pagedResult.getContent();
			} else {
				return new ArrayList<Invoice>();
	    	}
		} catch (Exception e) {
			log.error("Unable to find invoices with year: {}", year);
			throw new ServiceException(e);
		}
	}

	
	@Override
	public List<Invoice> findAllByAmountDueBetween(double min, double max, Integer page, Integer size, String sort) {
		try {
			Pageable pageable = PageRequest.of(page, size, Sort.by(sort));
			Page<Invoice> pagedResult = invRep.findAllByAmountDueBetween(min, max, pageable);
			if(pagedResult.hasContent()) {
				return pagedResult.getContent();
			} else {
				return new ArrayList<Invoice>();
	    	}
		} catch (Exception e) {
			log.error("Unable to find invoices with amounts between {} and {}", min, max);
			throw new ServiceException(e);
		}
	}


	@Override
	public Invoice findById(int id) {
		try {
			return invRep.findById(id).get();
		} catch (Exception e) {
			log.error("Unable to find invoice whith id: {}", id);
			throw new EntityNotFoundException(id, Invoice.class);
		}
	}




}

