
/**
 * Fattura emessa al cliente
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.models.invoice;

import java.time.LocalDate;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIncludeProperties;

import it.leonardograsso.epicenergyservices.models.BaseEntity;
import it.leonardograsso.epicenergyservices.models.customer.CompanyDataItem;
import it.leonardograsso.epicenergyservices.models.customer.Customer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity (name = "invoices")
public class Invoice extends BaseEntity implements CompanyDataItem{
	
	/** 
	 * cliente a cui è riferita la fattura
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private Customer customer;
	
	/**
	 * anno di emissione
	 */
	@Column(length = 4, nullable = false)
	private int year;
	
	/**
	 * data di emissione
	 */
	private LocalDate date;
	
	/**
	 * importo della fattura
	 */
	@Column(nullable = false)
	private double amountDue;
	
	/**
	 * numero della fattura, deve essere univoco
	 */
	@Column(length = 10, unique = true, nullable = false)
	private int number;
	
	
	/**
	 * stato della fattura,
	 * deve essere possibile estendere le casistiche 
	 * di questo attributo dinamicamente
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIncludeProperties(value = { "statusName" })
	private InvoiceStatus status;
	
	
	
	
	
	
	
	

}
