package it.leonardograsso.epicenergyservices.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.leonardograsso.epicenergyservices.models.user.Role;



public interface RoleRepository extends JpaRepository<Role, Long> {
	/**
	 * Ricerca del ruolo per nome.
	 */
	Optional<Role> findByName(String roleName);

}
