
/**
 * Gestione dei numeri di telefono.
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.services;

import java.util.Optional;

import it.leonardograsso.epicenergyservices.models.customer.PhoneNumber;

public interface PhoneNumberService extends BaseService{
	
	/**
	 * salva un nuovo numero telefonico
	 * 
	 * @param phoneNumber   numero telefonico
	 */
	PhoneNumber savePhoneNumber(PhoneNumber phoneNumber);

	
	/**
	 * modifica un numero telefonico
	 * 
	 * @param id			id numero telefonico
	 * @param phoneNumber	numero telefonico
	 * @return				numero telefonico
	 */
	PhoneNumber updatePhoneNumber(int id, PhoneNumber phoneNumber);

	
	/**
	 * cancella numero telefonico
	 * 
	 * @param id
	 * @return numero telefonico cancellato
	 */
	PhoneNumber delitePhoneNumber(int id);
	
	
	/**
	 * ricerca numero telefonico per id
	 * 
	 * @param id	id numero telefonico
	 * @return		numero telefonico
	 */
	public Optional<PhoneNumber> findById(int id);

}
