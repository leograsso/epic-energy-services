package it.leonardograsso.epicenergyservices.repositories;

/**
 * Repository per email
 * 
 * @author LeoGrasso
 *
 */

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.leonardograsso.epicenergyservices.models.customer.Email;

@Repository
public interface EmailRepository extends JpaRepository<Email, Integer>{

}
