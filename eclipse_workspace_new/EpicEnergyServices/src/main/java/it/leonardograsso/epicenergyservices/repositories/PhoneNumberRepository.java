package it.leonardograsso.epicenergyservices.repositories;

/**
 * Repository per contatti telefonici
 * 
 * @author LeoGrasso
 *
 */

import org.springframework.data.jpa.repository.JpaRepository;



import org.springframework.stereotype.Repository;

import it.leonardograsso.epicenergyservices.models.customer.PhoneNumber;

@Repository
public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, Integer>{

}
