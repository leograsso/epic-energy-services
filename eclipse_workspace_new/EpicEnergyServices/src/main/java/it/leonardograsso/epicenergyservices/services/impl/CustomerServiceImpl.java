/**
 * Business Service della classe Customer
 * 
 * @author LeoGrasso
 * 
 */

package it.leonardograsso.epicenergyservices.services.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import it.leonardograsso.epicenergyservices.models.customer.Customer;
import it.leonardograsso.epicenergyservices.repositories.CustomerRepository;
import it.leonardograsso.epicenergyservices.services.CustomerService;
import it.leonardograsso.epicenergyservices.services.exceptions.EntityNotFoundException;
import it.leonardograsso.epicenergyservices.services.exceptions.ServiceException;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);
	
	@Autowired
	private CustomerRepository cr;

	
	
	@Override
	public Customer saveCustomer(Customer customer) {
		try {
			Customer newCustomer = new Customer();
			newCustomer = customer;
			//setta la data di creazione come data di ultimo contatto
			newCustomer.setLastContactDate(LocalDate.now());
			return cr.save(newCustomer);
		} catch (Exception e) {
			log.error("Unable to save the customer {}", customer.getCompanyName());
			throw new ServiceException(e);
		}
	}

	
	@Override
	public Customer updateCustomer(int id, Customer customer) {
		try {
			var c = cr.findById(id).orElseThrow(() -> new EntityNotFoundException(id, Customer.class));
			c.setCompanyName(customer.getCompanyName());
			c.setVat(customer.getVat());
			c.setEmails(customer.getEmails());
			c.setPhoneNumbers(customer.getPhoneNumbers());
			c.setCreationDate(customer.getCreationDate());
			//aggiorna la data a quella dell'ultima modifica
			c.setLastContactDate(LocalDate.now());
			c.setInvoice(customer.getInvoice());
			c.setCorporateForm(customer.getCorporateForm());
			c.setAddresses(customer.getAddresses());
			c.setRevenue(customer.getRevenue());
			c.setReferenceContacts(customer.getReferenceContacts());
			return cr.save(c);
		} catch (EntityNotFoundException e) {
			if (e.getType().equals(Customer.class))
				log.error("Unable to find company {} whit VAT {}", customer.getCompanyName(), customer.getVat());
				throw e;
		} catch (Exception e) {
			log.error("Unable to update company {}", customer.getCompanyName());
			throw new ServiceException(e);
		}
	}

	
	@Override
	public Customer deleteCustomer(int id) {
		try {
			Customer c = cr.findById(id).orElseThrow(() -> new EntityNotFoundException(id, Customer.class));
			cr.delete(c);
			return c;
		} catch (EntityNotFoundException e) {
			log.error("Unable to find company whith id {}", id);
			throw e;
		} catch (Exception e) {
			log.error("Unable to delete company whith id {}", id);
			throw new ServiceException(e);
		}
	}

	
	@Override
	public List<Customer> findAllByRevenue(String revenue, Integer page, Integer size, String sort) {
		try {
			Pageable pageable = PageRequest.of(page, size, Sort.by(sort));
			Page<Customer> pagedResult = cr.findAllByRevenue(revenue, pageable);
			if(pagedResult.hasContent()) {
				return pagedResult.getContent();
			} else {
				return new ArrayList<Customer>();
	    	}
		} catch (Exception e) {
			log.error("Unable to find companies whith revenue type: {}", revenue);
			throw new ServiceException(e);
		}

	}

	
	@Override
	public List<Customer> findAllByCreationDate
	(int day, int month, int year, Integer page, Integer size, String sort) {
		try {
			//crea istanza LocalDate
			LocalDate date = LocalDate.of(year, month, day);
			
			Pageable pageable = PageRequest.of(page, size, Sort.by(sort));
			Page<Customer> pagedResult = cr.findAllByCreationDate(date, pageable);
			if(pagedResult.hasContent()) {
				return pagedResult.getContent();
			} else {
				return new ArrayList<Customer>();
	    	}
		} catch (Exception e) {
			log.error("Unable to find companies");
			throw new ServiceException(e);
		}
	}

	
	@Override
	public List<Customer> findAllByLastContactDate
	(int day, int month, int year, Integer page, Integer size, String sort) {
		try {
			//crea istanza LocalDate
			LocalDate date = LocalDate.of(year, month, day);
			
			Pageable pageable = PageRequest.of(page, size, Sort.by(sort));
			Page<Customer> pagedResult = cr.findAllByLastContactDate(date, pageable);
			if(pagedResult.hasContent()) {
				return pagedResult.getContent();
			} else {
				return new ArrayList<Customer>();
	    	}
		} catch (Exception e) {
			log.error("Unable to find companies");
			throw new ServiceException(e);
		}

	}

	
	@Override
	public Customer findByCompanyNameIgnoreCase(String companyName) {
		try {
			return cr.findByCompanyNameIgnoreCase(companyName);
		} catch (Exception e) {
			log.error("Unable to find companies whith name: {}", companyName);
			throw new EntityNotFoundException(companyName, Customer.class);
		}
	}


	@Override
	public Optional<Customer> findById(int id) {
		try {
			return cr.findById(id);
		} catch (Exception e) {
			log.error("Unable to find companies whith id: {}", id);
			throw new EntityNotFoundException(id, Customer.class);
		}
	}
	
	

}
