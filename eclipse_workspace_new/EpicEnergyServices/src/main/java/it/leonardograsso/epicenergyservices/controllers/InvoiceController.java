
/**
 * REST Controller 
 * Gestisce le request per la classe Invoice
 * @author LeoGrasso
 */

package it.leonardograsso.epicenergyservices.controllers;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.leonardograsso.epicenergyservices.models.invoice.Invoice;
import it.leonardograsso.epicenergyservices.services.impl.InvoiceServiceImpl;

@RestController
@RequestMapping("/invoice")
public class InvoiceController {
	
	@Autowired
	private InvoiceServiceImpl invoiceServ;
	
	
	/*
	 * Classe Message di tipo Utility
	 * Raccoglie i messaggi di errore delle eccezioni
	 * Il costruttore privato mi assicura che questa classe non possa essere istanziata
	 */
	protected static class Messages {
		public static final String ERROR_SAVE = "An unexpected error occurred while saving";
		public static final String ERROR_UPDATE = "An unexpected error occurred while updating";
		public static final String ERROR_DELETE = "An unexpected error occurred while deleting";
		public static final String ERROR_LIST = "An error occurred while creating the customer list";
		
		

		  private Messages() {
			    throw new IllegalStateException("Utility class");
			  }
	}
	
	static Logger log = LoggerFactory.getLogger(InvoiceController.class);
	
	
	/**
	 * Gestisce richiesta POST inserimento nuova fattura
	 * 
	 * @param customerId	id del cliente a cui è riferita la fattura
	 * @param invoice		riceve un oggetto Invoice in formato Json
	 * @return				un oggetto Invoice in formato Json
	 */
	@PostMapping(value = "/add/{customerId}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<Invoice> addInvoice(@PathVariable int customerId, @RequestBody Invoice invoice){
		try {
			return new ResponseEntity<>(invoiceServ.saveInvoice(customerId, invoice), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_SAVE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	/**
	 * Gestisce richiesta POST modifica di una fattura
	 * 
	 * @param invoiceId		id della fattura da modificare
	 * @param customerId	id del cliente a cui è riferita la fattura
	 * @param invoice		riceve un oggetto Invoice in formato Json
	 * @return				un oggetto Invoice in formato Json
	 */
	@PostMapping(value = "/update/{id}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<Invoice> updateInvoice(@PathVariable int id, @RequestBody Invoice invoice){
		try {
			return new ResponseEntity<>(invoiceServ.updateInvoice(id, invoice), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_SAVE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	/**
	 * Gestisce richiesta GET cancellazione fattura
	 * 
	 * @param id	id fattura da cancellare
	 * @return		l' oggetto fattura cancellata in formato Json
	 */
	@GetMapping(value = "/delete/{id}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<Invoice> deleteInvoice(@PathVariable int id){
		try {
			return new ResponseEntity<>(invoiceServ.deleteInvoice(id), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_DELETE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	/**
	 * 
	 * Gestisce richiesta GET per ottenere la lista delle fatture di un cliente
	 * 
	 * @param id		id cliente
	 * 
	 * @param page		numero di pagine 
	 * 
	 * @param size		numero di elementi per pagina
	 * 
	 * @param sort		ordinamento che può avere i seguenti valori: 
	 * 					"year"		--> anno 
	 * 					"date"		-->	data emissione fattura
	 * 					"amountDue"	--> importo
	 * 					"number"	--> numero fattura
	 * 					"status"	--> stato della fattura
	 * 
	 * @return			lista di fatture con paginazione ed ordinamento personalizzati
	 */
	@GetMapping(value = "/getbycustomerid")
	@PreAuthorize("hasRole('admin') or hasRole('user')")
	public ResponseEntity<List<Invoice>> getAllByCustomerId(@RequestParam int id, @RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "1") Integer size, @RequestParam(defaultValue = "id") String sort){
		
		try {
			List<Invoice> invoices = invoiceServ.findAllByCustomerId(id, page, size, sort);
			return new ResponseEntity<>(invoices, HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_LIST, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	/**
	 * 
	 * Gestisce richiesta GET per ottenere la lista delle fatture avente lo stesso stato
	 * 
	 * @param id		id dello stato ricercato
	 * 					1 --> pagata
	 * 					2 --> non pagata
	 * 
	 * @param page		numero di pagine 
	 * 
	 * @param size		numero di elementi per pagina
	 * 
	 * @param sort		ordinamento che può avere i seguenti valori: 
	 * 					"year"		--> anno 
	 * 					"date"		-->	data emissione fattura
	 * 					"amountDue"	--> importo
	 * 					"number"	--> numero fattura
	 * 					"customer"	--> clienti
	 * 
	 * @return			lista di fatture con paginazione ed ordinamento personalizzati
	 */
	@GetMapping(value = "/getbystatus")
	@PreAuthorize("hasRole('admin') or hasRole('user')")
	public ResponseEntity<List<Invoice>> getAllByStatusId(@RequestParam int id, @RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "1") Integer size, @RequestParam(defaultValue = "id") String sort){
		
		try {
			List<Invoice> invoices = invoiceServ.findAllByStatusId(id, page, size, sort);
			return new ResponseEntity<>(invoices, HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_LIST, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	/**
	 * 
	 * Gestisce richiesta GET per ottenere la lista delle fatture emesse lo stesso giorno
	 * 
	 * @param day		giorno
	 * @parma month		mese
	 * @param year		anno
	 * 
	 * @param page		numero di pagine 
	 * 
	 * @param size		numero di elementi per pagina
	 * 
	 * @param sort		ordinamento che può avere i seguenti valori: 
	 * 					"year"		--> anno 
	 * 					"status"	-->	stato fattura
	 * 					"amountDue"	--> importo
	 * 					"number"	--> numero fattura
	 * 					"customer"	--> clienti
	 * 
	 * @return			lista di fatture con paginazione ed ordinamento personalizzati
	 */
	@GetMapping(value = "/getbydate")
	@PreAuthorize("hasRole('admin') or hasRole('user')")
	public ResponseEntity<List<Invoice>> getAllByDate(@RequestParam int day, @RequestParam int month, @RequestParam int year, @RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "1") Integer size, @RequestParam(defaultValue = "id") String sort){
		
		try {
			LocalDate date = LocalDate.of(year, month, day);
			List<Invoice> invoices = invoiceServ.findAllByDate(date, page, size, sort);
			return new ResponseEntity<>(invoices, HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_LIST, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	/**
	 * 
	 * Gestisce richiesta GET per ottenere la lista delle fatture emesse lo stesso anno
	 * 
	 * @param year		anno
	 * 
	 * @param page		numero di pagine 
	 * 
	 * @param size		numero di elementi per pagina
	 * 
	 * @param sort		ordinamento che può avere i seguenti valori: 
	 * 					"date"		--> data
	 * 					"status"	-->	stato fattura
	 * 					"amountDue"	--> importo
	 * 					"number"	--> numero fattura
	 * 					"customer"	--> clienti
	 * 
	 * @return			lista di fatture con paginazione ed ordinamento personalizzati
	 */
	@GetMapping(value = "/getbyyear")
	@PreAuthorize("hasRole('admin') or hasRole('user')")
	public ResponseEntity<List<Invoice>> getAllByYear(@RequestParam int year, @RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "1") Integer size, @RequestParam(defaultValue = "id") String sort){
		
		try {
			List<Invoice> invoices = invoiceServ.findAllByYear(year, page, size, sort);
			return new ResponseEntity<>(invoices, HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_LIST, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	
	/**
	 * 
	 * Gestisce richiesta GET per ottenere la lista delle fatture comprese fra 
	 * un importo min e max
	 * 
	 * @param min		importo minimo
	 * @param max		importo max
	 * 
	 * @param page		numero di pagine 
	 * 
	 * @param size		numero di elementi per pagina
	 * 
	 * @param sort		ordinamento che può avere i seguenti valori: 
	 * 					"date"		--> data
	 * 					"status"	-->	stato fattura
	 * 					"amountDue"	--> importo
	 * 					"number"	--> numero fattura
	 * 					"customer"	--> clienti
	 * 					"year"		--> anno di emissione
	 * 
	 * @return			lista di fatture con paginazione ed ordinamento personalizzati
	 */
	@GetMapping(value = "/getbetween")
	@PreAuthorize("hasRole('admin') or hasRole('user')")
	public ResponseEntity<List<Invoice>> getAllBetween(@RequestParam double min, @RequestParam double max, @RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "1") Integer size, @RequestParam(defaultValue = "id") String sort){
		try {
			List<Invoice> invoices = invoiceServ.findAllByAmountDueBetween(min, max, page, size, sort);
			return new ResponseEntity<>(invoices, HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_LIST, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

}
