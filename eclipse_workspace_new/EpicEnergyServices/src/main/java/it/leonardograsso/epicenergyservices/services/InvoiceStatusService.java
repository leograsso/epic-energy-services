
/**
 * Gestione dello stato delle fatture
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.services;

import it.leonardograsso.epicenergyservices.models.invoice.InvoiceStatus;

public interface InvoiceStatusService extends BaseService {
	
	/**
	 * Salva un nuovo stato sul database
	 * 
	 * @param status		stato fattura
	 * @param invoiceId		id fattura
	 * @return				stato fattura
	 */
	public InvoiceStatus addStatus(InvoiceStatus status);
	
	/**
	 * Cancella uno stato dal database
	 * 
	 * @param id	id dello status fattura
	 * @return		status eliminato
	 */
	public InvoiceStatus remuveStatus(int id);
	
	/**
	 * Restituisce uno stato fattura dall'id
	 * 
	 * @param id tipo stato fattura
	 * @return stato fattura
	 */
	public InvoiceStatus getById (int id);
	

}
