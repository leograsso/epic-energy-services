
/**
 * REST Controller 
 * Gestisce le request per la classe Address
 * @author LeoGrasso
 */

package it.leonardograsso.epicenergyservices.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.leonardograsso.epicenergyservices.models.customer.Address;
import it.leonardograsso.epicenergyservices.repositories.CustomerRepository;
import it.leonardograsso.epicenergyservices.services.impl.AddressServiceImpl;
import it.leonardograsso.epicenergyservices.util.cityloader.CityServiceImpl;

@RestController
@RequestMapping("/customer/address")
public class AddressController {
	
	@Autowired
	private AddressServiceImpl addressServ;
	
	@Autowired
	private CityServiceImpl cityServ;
	
	@Autowired
	private CustomerRepository customerRep;
	
	/*
	 * Classe Message di tipo Utility
	 * Raccoglie i messaggi di errore delle eccezioni
	 * Il costruttore privato mi assicura che questa classe non possa essere istanziata
	 */
	protected static class Messages {
		public static final String ERROR_SAVE = "An unexpected error occurred while saving";
		public static final String ERROR_UPDATE = "An unexpected error occurred while updating";
		public static final String ERROR_DELETE = "An unexpected error occurred while deleting";

		  private Messages() {
			    throw new IllegalStateException("Utility class");
			  }
	}
	
	static Logger log = LoggerFactory.getLogger(AddressController.class);
	
	
	/**
	 * Gestisce richiesta POST inserimento nuovo indirizzo
	 * @param customerId	id cliente
	 * @param address		riceve un oggetto Address in formato Json
	 * @return				un oggetto Address in formato Json
	 */
	@PostMapping(value = "/add/{customerId}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<Address> addAddress(@PathVariable int customerId, @RequestBody Address address){
		try {
			var city = cityServ.findByName(address.getCity().getName());
			var cust = customerRep.findById(customerId).get();
			address.setCity(city);
			address.setCustomer(cust);
			return new ResponseEntity<>(addressServ.saveAddress(address), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_SAVE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	/**
	 * Gestisce richiesta POST aggiornamento indirizzo
	 * @param Id	id indirizzo
	 * @param address		riceve un oggetto Address in formato Json
	 * @return				un oggetto Address in formato Json
	 */
	@PostMapping(value = "/update/{id}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<Address> updateAddress(@PathVariable int id, @RequestBody Address address){
		try {
			return new ResponseEntity<>(addressServ.updateAddress(id, address), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_UPDATE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	/**
	 * Gestisce richiesta GET cancellazione indirizzo
	 * @param Id	id indirizzo
	 * @return		l'oggetto Address cancellato in formato Json
	 */
	@GetMapping(value = "/delete/{id}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<Address> deleteAddress(@PathVariable int id){
		try {
			return new ResponseEntity<>(addressServ.deliteAddress(id), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_DELETE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	
	
	
	

}
