package it.leonardograsso.epicenergyservices.util.cityloader;

import javax.persistence.Column;
import javax.persistence.Entity;

import it.leonardograsso.epicenergyservices.models.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode (callSuper = true)
@NoArgsConstructor
@Entity (name = "cities")
public class CityModel extends BaseEntity {
	
	
	private String name;
	private String region;
	@Column(name = "is_province")
	private String isProvince;
	private String acronym;

}
