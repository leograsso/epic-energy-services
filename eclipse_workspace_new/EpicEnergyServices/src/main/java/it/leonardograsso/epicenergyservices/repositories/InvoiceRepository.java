
/**
 * Repository della classe Invoice
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.repositories;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.leonardograsso.epicenergyservices.models.invoice.Invoice;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Integer> {
	
	/**
	 * 
	 * @param customerId id del cliente associato alla fattura
	 * @param pageable implementa la paginazione
	 * @return lista di fatture filtrata per cliente
	 */
	Page<Invoice> findAllByCustomerId(int customerId, Pageable pageable);
	
	/**
	 * 
	 * @param statusId id dello stato fattura
	 * @param pageable implementa la paginazione
	 * @return lista di fatture filtrata per stato
	 */
	Page<Invoice> findAllByStatusId(int statusId, Pageable pageable);
	
	/**
	 * 
	 * @param date data di emissione della fattura
	 * @param pageable implementa la paginazione
	 * @return lista di fatture filtrata per data di emissione
	 */
	Page<Invoice> findAllByDate(LocalDate date, Pageable pageable);
	
	/**
	 * 
	 * @param year anno di emissione ricercato
	 * @param pageable implementa la paginazione
	 * @return lista di fatture filtrata per anno di emissione
	 */
	Page<Invoice> findAllByYear(int year, Pageable pageable);
	
	/**
	 * 
	 * @param min valore minimo dell'intervallo di ricerca
	 * @param max valore massimo dell'intervallo di ricerca
	 * @param pageable implementa la paginazione
	 * @return lista di fatture filtrata per intervallo di importo
	 */
	Page<Invoice> findAllByAmountDueBetween(double min, double max, Pageable pageable);
	
	
	
	

}
