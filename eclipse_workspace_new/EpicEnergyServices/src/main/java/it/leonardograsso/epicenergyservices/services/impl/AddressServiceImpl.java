/**
 * Business Service degli indirizzi
 * 
 * @author LeoGrasso
 * 
 */

package it.leonardograsso.epicenergyservices.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import it.leonardograsso.epicenergyservices.models.customer.Address;
import it.leonardograsso.epicenergyservices.models.customer.Address.AddressType;
import it.leonardograsso.epicenergyservices.repositories.AddressRepository;
import it.leonardograsso.epicenergyservices.services.AddressService;
import it.leonardograsso.epicenergyservices.services.exceptions.EntityNotFoundException;
import it.leonardograsso.epicenergyservices.services.exceptions.ServiceException;

@Service
public class AddressServiceImpl implements AddressService {
	
	Logger log = LoggerFactory.getLogger(AddressServiceImpl.class);
	
	@Autowired
	private AddressRepository ar;
	

	@Override
	public Address saveAddress(Address address) {
		try {
			//recupera il cliente
			var customer = address.getCustomer();
			//recupera id cliente
			var customerId = customer.getId();
			//se l'indirizzo è una sede legale imposta l'acronimo della provincia al cliente
			if(address.getType() == AddressType.REGISTERED_OFFICE) {
				customer.setProvince(address.getCity().getAcronym());
			}
			var typeList = ar.findTypeByCustomerId(customerId);
			//se non ci sono indirizzi corrispondenti al cliente salva l'indirizzo
			if (typeList.isEmpty()) {
				return ar.save(address);
			//se ci sono due indirizzi corrispondenti al cliente non salva
			} else if (typeList.size() == 2) {
				log.error("it is not possible to enter more than two addresses for each customer");
				return address;
			//se c'è un indirizzo nella lista index 0 ma è di tipo diverso a quello che si vuole salvare, lo salva
			} else if (typeList.size() == 1 && typeList.get(0).get().getType() != address.getType()) {
				return ar.save(address);	
			//se c'è un indirizzo nella lista index 0 ed è di tipo uguale a quello che si vuole salvare, non lo salva
			} else if (typeList.size() == 1 && typeList.get(0).get().getType() == address.getType()) {
				log.error("Unable to save the address, a this type address is already present");
				return address;
			//se c'è un indirizzo nella lista index 1 ma è di tipo diverso a quello che si vuole salvare, lo salva
			} else if (typeList.size() == 1 && typeList.get(1).get().getType() != address.getType()) {
				return ar.save(address);	
			//se c'è un indirizzo nella lista index 1 ed è di tipo uguale a quello che si vuole salvare, non lo salva
			} else if (typeList.size() == 1 && typeList.get(1).get().getType() == address.getType()) {
				log.error("Unable to save the address, a this type address is already present");
				return address;
			} else {
				log.error("Unable to save the address : {}, {}", address.getStreet(), address.getNumber());
				return address;
			}
		} catch (Exception e) {
			log.error("Unable to save the address : {}, {}", address.getStreet(), address.getNumber());
			throw new ServiceException(e);
		}
	}

	
	@Override
	public Address updateAddress(int id, Address address) {
		try {
			var a = ar.findById(id).orElseThrow(() -> new EntityNotFoundException(id, Address.class));
			a.setStreet(address.getStreet());
			a.setNumber(address.getNumber());
			a.setCustomer(address.getCustomer());
			a.setFraction(address.getFraction());
			a.setPostalCode(address.getPostalCode());
			a.setCity(address.getCity());
			return ar.save(a);
		} catch (EntityNotFoundException e) {
			if (e.getType().equals(Address.class))
				log.error("Unable to find the address : {}, {}", address.getStreet(), address.getNumber());
				throw e;
		} catch (Exception e) {
			log.error("Unable to update the address : {}, {}", address.getStreet(), address.getNumber());
			throw new ServiceException(e);
		}
	}

	
	@Override
	public Address deliteAddress(int id) {
		try {
			Address a = ar.findById(id).orElseThrow(() -> new EntityNotFoundException(id, Address.class));
			ar.delete(a);
			return a;
		} catch (EntityNotFoundException e) {
			log.error("Unable to find the address whith id: {}", id);
			throw e;
		} catch (Exception e) {
			log.error("Unable to delete address whith id {}", id);
			throw new ServiceException(e);
		}
	}
	
	
}



