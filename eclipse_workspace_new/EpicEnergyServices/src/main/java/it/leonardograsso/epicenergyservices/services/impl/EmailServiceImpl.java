
/**
 * Business Service degli indirizzi email
 * 
 * @author LeoGrasso
 * 
 */

package it.leonardograsso.epicenergyservices.services.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.leonardograsso.epicenergyservices.models.customer.Email;
import it.leonardograsso.epicenergyservices.repositories.EmailRepository;
import it.leonardograsso.epicenergyservices.services.EmailService;
import it.leonardograsso.epicenergyservices.services.exceptions.EntityNotFoundException;
import it.leonardograsso.epicenergyservices.services.exceptions.ServiceException;

@Service
public class EmailServiceImpl implements EmailService{
	
	Logger log = LoggerFactory.getLogger(EmailServiceImpl.class);
	
	@Autowired
	private EmailRepository er;

	@Override
	public Email saveEmail(Email email) {
		try {
			return er.save(email);
		} catch (Exception e) {
			log.error("Unable to save the email {}", email.getEmailAddress());
			throw new ServiceException(e);
		}
	}
	

	@Override
	public Email updateEmail(int id, Email email) {
		try {
			var e = er.findById(id).orElseThrow(() -> new EntityNotFoundException(id, Email.class));
			e.setCustomer(email.getCustomer());
			e.setEmailAddress(email.getEmailAddress());
			e.setReferenceContact(email.getReferenceContact());
			e.setType(email.getType());
			return er.save(e);
		} catch (EntityNotFoundException e) {
			if (e.getType().equals(Email.class))
				log.error("Unable to find the email {}", email.getEmailAddress());
				throw e;
		} catch (Exception e) {
			log.error("Unable to update the email {}", email.getEmailAddress());
			throw new ServiceException(e);
		}
	}

	
	@Override
	public Email deliteEmail(int id) {
		try {
			Email e = er.findById(id).orElseThrow(() -> new EntityNotFoundException(id, Email.class));
			er.delete(e);
			return e;
		} catch (EntityNotFoundException e) {
			log.error("Unable to find the email whith id: {}", id);
			throw e;
		} catch (Exception e) {
			log.error("Unable to delete email whith id: {}", id);
			throw new ServiceException(e);
		}
	}


	@Override
	public Optional<Email> findById(int id) {
		try {
			return er.findById(id);
		} catch (Exception e) {
			log.error("Unable to find email whith id: {}", id);
			throw new EntityNotFoundException(id, Email.class);
		}
	}
	

}
