
/**
 * Gestione dei clienti.
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.services;

import java.util.List;
import java.util.Optional;

import it.leonardograsso.epicenergyservices.models.customer.Customer;

public interface CustomerService extends BaseService{
	
	/**
	 * salvataggio di un cliente
	 * 
	 * @param customer cliente da salvare nel database
	 * @return un oggetto Customer
	 */
	public Customer saveCustomer(Customer customer);
	
	/**
	 * modifica di un cliente
	 * 
	 * @param id 			identificativo cliente
	 * @param customer 		cliente da modificare
	 * @return un oggetto 	Customer
	 */
	public Customer updateCustomer (int id, Customer customer);
		
	/**
	 * cancellazione di un cliente
	 * 
	 * @param id 			identificativo cliente
	 * @return un oggetto 	Customer
	 */
	public Customer deleteCustomer(int id);
	
	/**
	 * ricerca di clienti per fatturato, con paginazione ed ordinamento
	 * 
	 * @param revenue	tipo di azienda
	 * @param page		numero di pagine
	 * @param size		numero di elementi da visualizzare per pagina
	 * @param sort		attributo per l'ordinamento
	 * @return			lista clienti
	 */
	public List<Customer> findAllByRevenue(String revenue, Integer page, Integer size, String sort);
	
	/**
	 * ricerca di clienti per data di creazione, con paginazione ed ordinamento
	 * 
	 * @param day			giorno
	 * @param month			mese
	 * @param year			anno
	 * @param page			numero di pagine
	 * @param size			numero di elementi da visualizzare per pagina
	 * @param sort			attributo per l'ordinamento
	 * @return				lista clienti
	 */
	public List<Customer> findAllByCreationDate(int day, int month, int year, Integer page, Integer size, String sort);
	
	/**
	 * ricerca di clienti per data di ultimo contatto, con paginazione ed ordinamento
	 * 
	 * @param day			giorno
	 * @param month			mese
	 * @param year			anno
	 * @param page			numero di pagine
	 * @param size			numero di elementi da visualizzare per pagina
	 * @param sort			attributo per l'ordinamento
	 * @return				lista clienti
	 */
	public List<Customer> findAllByLastContactDate(int day, int month, int year, Integer page, Integer size, String sort);
	
	/**
	 * ricerca cliente per ragione sociale
	 * 
	 * @param companyName	ragione sociale, anche parziale
	 * @return				cliente
	 */
	public Customer findByCompanyNameIgnoreCase(String companyName);
	
	/**
	 * ricerca cliente per id
	 * 
	 * @param id	id cliente
	 * @return		cliente
	 */
	public Optional<Customer> findById(int id);
	
	

}
