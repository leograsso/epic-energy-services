
/**
 * Business Service dei referenti aziendali
 * 
 * @author LeoGrasso
 * 
 */


package it.leonardograsso.epicenergyservices.services.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.leonardograsso.epicenergyservices.models.customer.ReferenceContact;
import it.leonardograsso.epicenergyservices.repositories.ReferenceContactRepository;
import it.leonardograsso.epicenergyservices.services.ReferenceContactService;
import it.leonardograsso.epicenergyservices.services.exceptions.EntityNotFoundException;
import it.leonardograsso.epicenergyservices.services.exceptions.ServiceException;

@Service
public class ReferenceContactServiceImpl implements ReferenceContactService {
	
Logger log = LoggerFactory.getLogger(ReferenceContactServiceImpl.class);
	
	@Autowired
	private ReferenceContactRepository rcr;

	@Override
	public ReferenceContact saveReferenceContact(ReferenceContact rc) {
		try {
			return rcr.save(rc);
		} catch (Exception e) {
			log.error("Unable to save the Reference Contact: {} {}", rc.getName(), rc.getSurname());
			throw new ServiceException(e);
		}
	}

	
	@Override
	public ReferenceContact updateReferenceContact(int id, ReferenceContact rc) {
		try {
			var r = rcr.findById(id).orElseThrow(() -> new EntityNotFoundException(id, ReferenceContact.class));
			r.setCustomer(rc.getCustomer());
			r.setName(rc.getName());
			r.setSurname(rc.getSurname());
			r.setEmail(rc.getEmail());
			r.setPhoneNumber(rc.getPhoneNumber());
			r.setCompanyRole(rc.getCompanyRole());
			return rcr.save(r);
		} catch (EntityNotFoundException e) {
			if (e.getType().equals(ReferenceContact.class))
				log.error("Unable to find the Reference Contact : {} {}", rc.getName(), rc.getSurname());
				throw e;
		} catch (Exception e) {
			log.error("Unable to update the Reference Contact : {} {}", rc.getName(), rc.getSurname());
			throw new ServiceException(e);
		}
	}


	@Override
	public ReferenceContact deliteReferenceContact(int id) {
		try {
			ReferenceContact r = rcr.findById(id).orElseThrow(() -> new EntityNotFoundException(id, ReferenceContact.class));
			rcr.delete(r);
			return r;
		} catch (EntityNotFoundException e) {
			log.error("Unable to find the Reference Contact whith id : {}", id);
			throw e;
		} catch (Exception e) {
			log.error("Unable to delete Reference Contact whith id {}", id);
			throw new ServiceException(e);
		}
	}


	@Override
	public Optional<ReferenceContact> findById(int id) {
		try {
			return rcr.findById(id);
		} catch (Exception e) {
			log.error("Unable to find reference contact whith id: {}", id);
			throw new EntityNotFoundException(id, ReferenceContact.class);
		}
	}
	
	
	
	
	
	
	

}
