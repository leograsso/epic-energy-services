
/**
 * Repository della classe Customer
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.repositories;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.leonardograsso.epicenergyservices.models.customer.Customer;

@Repository
public interface CustomerRepository extends JpaRepository <Customer, Integer> {

	/**
	 * 
	 * @param revenue tipo di azienda per fatturato
	 * @param pageable implementa la paginazione
	 * @return lista di clienti filtrata per fatturato annuale
	 */
	@Query(value = "SELECT * FROM customers WHERE revenue=?1", nativeQuery = true)
	Page<Customer> findAllByRevenue(String revenue, Pageable pageable);
	
	/**
	 * 
	 * @param creationDate data
	 * @param pageable implementa la paginazione
	 * @return lista di clienti filtrata per data di creazione del profilo
	 */
	Page<Customer> findAllByCreationDate(LocalDate creationDate, Pageable pageable);
	
	/**
	 * 
	 * @param lastContactDate data
	 * @param pageable implementa la paginazione
	 * @return lista di clienti filtrata per data di ultimo contatto
	 */
	Page<Customer> findAllByLastContactDate(LocalDate lastContactDate, Pageable pageable);
	
	/**
	 * 
	 * @param companyName tutta o parte la ragione sociale
	 * @return cliente per ragione sociale
	 */
	Customer findByCompanyNameIgnoreCase(String companyName);
	
	/**
	 * 
	 * @param vat partita iva
	 * @return cliente per partita iva
	 */
	Customer findByVat(int vat);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
