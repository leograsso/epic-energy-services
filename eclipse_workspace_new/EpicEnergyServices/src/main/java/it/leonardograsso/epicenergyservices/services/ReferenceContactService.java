
/**
 * Gestione dei referenti aziendali.
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.services;

import java.util.Optional;

import it.leonardograsso.epicenergyservices.models.customer.ReferenceContact;

public interface ReferenceContactService extends BaseService{
	
	/**
	 * salva un nuovo referente aziendale
	 * 
	 * @param rc   	referente aziendale
	 * @return		referente aziendale
	 */
	ReferenceContact saveReferenceContact(ReferenceContact rc);

	
	/**
	 * modifica un referente aziendale
	 * 
	 * @param id	id referente aziendale
	 * @param rc	referente aziendale
	 * @return		referente aziendale
	 */
	ReferenceContact updateReferenceContact(int id, ReferenceContact rc);

	
	/**
	 * cancella referente aziendale
	 * 
	 * @param id
	 * @return referente aziendale cancellato
	 */
	ReferenceContact deliteReferenceContact(int id);
	
	
	/**
	 * ricerca referente per id
	 * 
	 * @param id	id referente
	 * @return		referente
	 */
	public Optional<ReferenceContact> findById(int id);

}

