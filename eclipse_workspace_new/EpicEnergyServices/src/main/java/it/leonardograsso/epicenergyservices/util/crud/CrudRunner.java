package it.leonardograsso.epicenergyservices.util.crud;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import it.leonardograsso.epicenergyservices.models.customer.Address;
import it.leonardograsso.epicenergyservices.models.customer.Address.AddressType;
import it.leonardograsso.epicenergyservices.models.customer.Customer;
import it.leonardograsso.epicenergyservices.models.customer.Customer.CorporateForm;
import it.leonardograsso.epicenergyservices.models.customer.Customer.TypeByRevenue;
import it.leonardograsso.epicenergyservices.models.customer.Email;
import it.leonardograsso.epicenergyservices.models.customer.Email.EmailType;
import it.leonardograsso.epicenergyservices.repositories.CustomerRepository;
import it.leonardograsso.epicenergyservices.services.impl.AddressServiceImpl;
import it.leonardograsso.epicenergyservices.services.impl.CustomerServiceImpl;
import it.leonardograsso.epicenergyservices.services.impl.EmailServiceImpl;
import it.leonardograsso.epicenergyservices.util.cityloader.CityRepository;
import it.leonardograsso.epicenergyservices.util.cityloader.CityService;
import it.leonardograsso.epicenergyservices.util.cityloader.CityServiceImpl;

@Component
@Order(value = 2)
public class CrudRunner implements CommandLineRunner {

	@Autowired
	CustomerRepository cr;
	
	@Autowired
	CityRepository cmr;
	
	@Autowired
	CityServiceImpl csimp;
	
	@Autowired
	CustomerServiceImpl csi;
	
	@Autowired
	EmailServiceImpl esi;
	
	@Autowired
	AddressServiceImpl asi;
	
	@Override
	public void run(String... args) throws Exception {
		
		Customer cs = new Customer();
		Email email = new Email().builder().customer(cs).emailAddress("bialetti@bialetti.it").type(EmailType.BUSINESS).build();
		
		Address a1 = new Address();
		a1.setStreet("Via Galileo");
		a1.setNumber(55);
		a1.setCustomer(cr.findById(7).get());
		a1.setCity(csimp.findByName("Riposto"));
		a1.setType(AddressType.REGISTERED_OFFICE);
		
		//asi.saveAddress(a1);
		
		
		
		cs.setCompanyName("Bialetti");
		cs.setVat(1234567891);
		cs.setCorporateForm(CorporateForm.SPA);
		cs.setRevenue(TypeByRevenue.GRANDE_IMP_OVER50ML);
		//csi.saveCustomer(cs);
		//esi.saveEmail(email);
		
		//csi.findAllByCreationDate(LocalDate.now(), 1, 1, "id");
		
		//csi.deleteCustomer(1);
		
		//System.out.println(cr.findByVat(447585417).getCompanyName());
		
		//asi.saveAddress(a1);
		
		
		
	}

}
