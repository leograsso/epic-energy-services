package it.leonardograsso.epicenergyservices.services.exceptions;

/**
 * Base per tutte le eccezioni di applicazione.
 * 
 * @author LeoGrasso
 *
 */

public class AppException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AppException() {
		super();
	}

	public AppException(Throwable cause) {
		super(cause);
	}

}
