
/**
 * Classe model dello user 
 * dell'applicazione
 * 
 * @author LeoGrasso
 */

package it.leonardograsso.epicenergyservices.models.user;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.leonardograsso.epicenergyservices.models.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity (name = "users")
public class User extends BaseEntity {
	
	/**
	 * nome dell'utente
	 */
	@Column(length = 20, nullable = false)
	private String name;
	
	/**
	 * cognome dell'utente
	 */
	private String surname;
	
	/**
	 * email usata come username 
	 * per l'accesso all'applicazione
	 */

	// lunghezza del campo cifrato tramite AES = 16 x (length / 16 + 1) =
	@Column(length = 112, nullable = false, unique = true)
	//@Convert(converter = SecureStringConverter.class)
	private String email;
	
	/**
	 * Password.
	 * 
	 * La lunghezza del campo tiene conto della lunghezza di una password codificata con l'algoritmo BCrypt.
	 */
	@Column(length = 60)
	@JsonIgnore
	private String password;
	
	/**
	 * stato dell'utente
	 * attivo/non attivo
	 */
	private boolean isActive = true;
	
	
	/**
	 * Ruoli di appartenenza.
	 */
	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private final Set<Role> roles = new HashSet<>();
	

	}
	



