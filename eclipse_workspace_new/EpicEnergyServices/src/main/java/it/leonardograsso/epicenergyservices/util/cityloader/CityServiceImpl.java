
/**
 * Classe service dell'entity City
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.util.cityloader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.leonardograsso.epicenergyservices.services.exceptions.ServiceException;

@Service
public class CityServiceImpl implements CityService {
	
	static Logger log = LoggerFactory.getLogger(CityServiceImpl.class);
	
	@Autowired
	private CityRepository city;
		
	@Override
	public String cityLoader() {
			FileReader fr;
			try {
				fr = new FileReader("src/main/resources/cities.csv");
				@SuppressWarnings("resource")
				BufferedReader br = new BufferedReader (fr);
				var cities = city.findAll();
				
				if(cities.iterator().hasNext()) {
					return "Cities are alredy present";
				} else {
					String line = "";
					while((line = br.readLine())!=null) {
						String [] data = line.split(";");
						var c = new CityModel();
						c.setName(data[0]);
						c.setRegion(data[1]);
						c.setIsProvince(data[2]);
						c.setAcronym(data[3]);
						city.save(c);
						return "The cities were successfully loaded into the database";
					}
				}		
			} catch (FileNotFoundException e) {
				log.error("File cities.csv not found");
				throw new ServiceException(e); 
			} catch (IOException e) {
				log.error("file reading or writing error");
				e.printStackTrace();
			}
			return null;
	}

	
	@Override
	public CityModel findByName(String name) {
		try {
			if(city.findCityModelByNameIgnoreCase(name).isPresent()) {
				return city.findCityModelByNameIgnoreCase(name).get();
			} else {
				log.error("City ({}) not found", name);
				return null;
			}
		} catch (Exception e) {
			log.error("Unable to update the city ({})", name);
			throw new ServiceException(e);
		}
		
	
		
	}
}
