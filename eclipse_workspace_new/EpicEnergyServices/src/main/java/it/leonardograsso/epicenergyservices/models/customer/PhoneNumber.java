/**
 * Classe che rappresenta un numero di telefono
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.models.customer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.leonardograsso.epicenergyservices.models.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity (name = "phone_numbers")
public class PhoneNumber extends BaseEntity {
	
	/**
	 * azienda cliente a cui è riferito il numero
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private Customer customer;
	
	/**
	 * numero diretto di un referente dell'azienda
	 */
	@OneToOne (mappedBy = "email")
	@JsonIgnore
	private ReferenceContact referenceContact;
	
	/**
	 * numero di telefono
	 * si da per scontato il prefisso italiano
	 * deve essere univoco
	 */
	@Column(length = 15, unique = true)
	private Long number;
	
	/**
	 * tipo di numero: fisso o mobile
	 */
	@Enumerated(EnumType.STRING)
	private NumberType type;
	
		public enum NumberType {
			MOBILE, LANDLINE
		}

}
