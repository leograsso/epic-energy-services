
/**
 * Intefaccia service dell' entity City
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.util.cityloader;



public interface CityService {
	
	/**
	 * Il metodo cityLoader() usa le classi FileReader e BufferedReader per leggere 
	 * il file contenente i dati delle città italiane e le carica sul database,
	 * è necessario che vi sia un file denominato cities.csv nel percorso src/main/resources dell'app.
	 */
	public String cityLoader();
	
	/**
	 * 
	 * @param name	nome della città
	 * @return		restituisce una città
	 */
	public CityModel findByName (String name);

}
