
/**
 * Classe che rappresenta il cliente
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.models.customer;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import it.leonardograsso.epicenergyservices.models.BaseEntity;
import it.leonardograsso.epicenergyservices.models.invoice.Invoice;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity (name = "customers")
public class Customer extends BaseEntity implements CompanyDataItem {
	
	
	/**
	 * ragione sociale
	 */
	@Column(length = 100, nullable = false)
	private String companyName;
	
	/**
	 * partita IVA aziendale,
	 * Si da per scontato il prefisso IT visto che trattasi
	 * di un gestionale per clienti italiani;
	 */
	@Column(length = 12, unique = true, nullable = false)
	private int vat;
	
	/**
	 * set di emails aziendale
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "customer", cascade = CascadeType.ALL)
	private Set<Email> emails = new HashSet<>();
	
	/**
	 * indirizzi aziendali
	 * dovrà essere possibile inserire un indirizzo per la sede legale
	 * ed uno per la sede operativa
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "customer", cascade = CascadeType.ALL)
	private Set<Address> addresses = new HashSet<>();
	
	/**
	 * set di numeri telefoni aziendali
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "customer", cascade = CascadeType.ALL)
	private Set<PhoneNumber> phoneNumbers = new HashSet<>();
	
	/**
	 * data di creazione del profilo cliente
	 * si inizializza automaticamente alla creazione
	 * di un nuovo cliente
	 */
	private LocalDate creationDate = LocalDate.now();
	
	/**
	 * data ultimo contatto/ultima modifica
	 */
	private LocalDate lastContactDate;
	
	/**
	 * set fatture emesse nei confronti del cliente
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "customer", cascade = CascadeType.ALL)
	private Set<Invoice> invoice = new HashSet<>();
	
	/**
	 * forme societarie secondo la legge italiana
	 */
	@Enumerated (EnumType.STRING)
	private CorporateForm corporateForm;
	/**
	 * Secondo le norme italiane le imprese possono avere le seguenti forme:
		SS:		società semplice
		SNA:	società in nome collettivo
		SAS:	società in accomandita semplice
		SRL: 	società a responsabilità limitata
		SPA: 	società per azioni
		SAPA: 	società in accomandita per azioni
		COOP:	cooperativa
		PA:		pubblica amministrazione

	 *
	 */
		public enum CorporateForm {
			PA, SS, SNC, SAS, SRL, SPA, SAPA, COOP
		}
	
	/**
	* fatturato aziendale
	* è valutato secondo la denominazione standard di 
	* micro, piccola, media e grande impresa
	*/
	@Enumerated (EnumType.STRING)
	private TypeByRevenue revenue;
		/**
		 * Secondo le norme comunitarie le imprese si differenziano in:

			micro imprese:		fatturato non superiore 2 milioni di euro;
			piccole imprese:	fatturato non superiore 10 milioni di euro;
			medie imprese:		fatturato non superiore 50 milioni di euro;
			grandi imprese:		fatturato superiore a 50 milioni di euro;

		 * @author LeoGrasso
		 *
		 */
		public enum TypeByRevenue {
			MICRO_IMP_MAX2ML, PICCOLA_IMP_MAX10ML, MEDIA_IMP_MAX50ML, GRANDE_IMP_OVER50ML
		}
	
	/**
	* contatti di riferimento all'interno dell'azienda
	*/
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "customer", cascade = CascadeType.ALL)
	private List <ReferenceContact> referenceContacts;
	
	/**
	 * acronimo della provincia
	 * della sede legale
	 * 
	 * viene impostato automaticamente
	 * quando viene creato un indirizzo avente come 
	 * tipo una sede legale
	 */
	@Column(length = 2)
	private String province;


	
	
	
	
	

}
