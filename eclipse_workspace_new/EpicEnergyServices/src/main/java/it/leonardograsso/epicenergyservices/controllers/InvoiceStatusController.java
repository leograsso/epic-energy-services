
/**
 * REST Controller 
 * Gestisce le request per la classe InvoiceStatus
 * @author LeoGrasso
 */

package it.leonardograsso.epicenergyservices.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.leonardograsso.epicenergyservices.models.invoice.InvoiceStatus;
import it.leonardograsso.epicenergyservices.services.impl.InvoiceStatusServiceImpl;

@RestController
@RequestMapping("/invoice/status")
public class InvoiceStatusController {
	
	@Autowired
	private InvoiceStatusServiceImpl invStatus;
	
	
	/*
	 * Classe Message di tipo Utility
	 * Raccoglie i messaggi di errore delle eccezioni
	 * Il costruttore privato mi assicura che questa classe non possa essere istanziata
	 */
	protected static class Messages {
		public static final String ERROR_SAVE = "An unexpected error occurred while saving";
		public static final String ERROR_UPDATE = "An unexpected error occurred while updating";
		public static final String ERROR_DELETE = "An unexpected error occurred while deleting";
		public static final String ERROR_LIST = "An error occurred while creating the customer list";
		
		

		  private Messages() {
			    throw new IllegalStateException("Utility class");
			  }
	}
	
	static Logger log = LoggerFactory.getLogger(InvoiceStatusController.class);
	
	
	/**
	 * Gestisce richiesta POST inserimento nuovo stato fattura
	 * I tipi di stato sono univoci
	 * Se lo stato esiste già non verrà inserito
	 * 
	 * @param status		riceve un oggetto InvoiceStatus in formato Json
	 * @return				un oggetto InvoiceStatus in formato Json
	 */
	@PostMapping(value = "/add")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<InvoiceStatus> addStatus(@RequestBody InvoiceStatus status){
		try {
			return new ResponseEntity<>(invStatus.addStatus(status), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_SAVE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
