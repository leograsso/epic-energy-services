
/**
 * REST Controller 
 * Gestisce le request per la classe User
 * @author LeoGrasso
 */

package it.leonardograsso.epicenergyservices.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.leonardograsso.epicenergyservices.models.user.User;
import it.leonardograsso.epicenergyservices.services.impl.UsersServiceImpl;


@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UsersServiceImpl userService;

	
	/*
	 * Classe Message di tipo Utility
	 * Raccoglie i messaggi di errore delle eccezioni
	 * Il costruttore privato mi assicura che questa classe non possa essere istanziata
	 */
	protected static class Messages {
		public static final String ERROR_SAVE = "An unexpected error occurred while saving";
		public static final String ERROR_UPDATE = "An unexpected error occurred while updating";
		public static final String ERROR_DELETE = "An unexpected error occurred while deleting";
		public static final String AUTH_EXCEPTION = "Service exception authenticating user";
		public static final String AUTH_UNATTENDED_EXCEPTION = "Unattended exception authenticating user";
		
		
		  private Messages() {
			    throw new IllegalStateException("Utility class");
			  }
	}
	
	static Logger log = LoggerFactory.getLogger(UserController.class);
	
	/**
	 * 
	 * @param user	nuovo utente
	 * @param role	può assumere 3 valori
	 * 				1 --> per assegnare ruolo admin
	 * 				2 --> per assegnare ruolo user
	 * 				3 --> per assegnare entrambi i ruoli
	 * @return		utente registrato
	 */
	@PostMapping("/add/{role}")
	public ResponseEntity<User> saveUser (@RequestBody User user, @PathVariable int role){
		try {
			userService.register(user, role);
			return new ResponseEntity<>(user, HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_SAVE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		
	}
	
	
	
	
}
