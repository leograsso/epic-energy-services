
/**
 * REST Controller 
 * Gestisce le request per la classe Reference Contact
 * @author LeoGrasso
 */

package it.leonardograsso.epicenergyservices.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.leonardograsso.epicenergyservices.models.customer.ReferenceContact;
import it.leonardograsso.epicenergyservices.services.impl.CustomerServiceImpl;
import it.leonardograsso.epicenergyservices.services.impl.ReferenceContactServiceImpl;

@RestController
@RequestMapping("/customer/refcontact")
public class ReferenceContactController {
	
	@Autowired
	private ReferenceContactServiceImpl referServ;
	
	@Autowired
	private CustomerServiceImpl customerServ;
	
	
	/*
	 * Classe Message di tipo Utility
	 * Raccoglie i messaggi di errore delle eccezioni
	 * Il costruttore privato mi assicura che questa classe non possa essere istanziata
	 */
	protected static class Messages {
		public static final String ERROR_SAVE = "An unexpected error occurred while saving";
		public static final String ERROR_UPDATE = "An unexpected error occurred while updating";
		public static final String ERROR_DELETE = "An unexpected error occurred while deleting";

		  private Messages() {
			    throw new IllegalStateException("Utility class");
			  }
	}
	
	static Logger log = LoggerFactory.getLogger(ReferenceContactController.class);
	
	
	/**
	 * Gestisce richiesta POST inserimento nuovo contatto aziendale
	 * @param customerId	id cliente
	 * @param reference		riceve un oggetto ReferenceContact in formato Json
	 * @return				un oggetto ReferenceContact in formato Json
	 */
	@PostMapping(value = "/add/{customerId}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<ReferenceContact> addContact(@PathVariable int customerId, @RequestBody ReferenceContact referenceContact){
		try {
			var customer = customerServ.findById(customerId);
			referenceContact.setCustomer(customer.get());
			return new ResponseEntity<>(referServ.saveReferenceContact(referenceContact), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_SAVE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	/**
	 * Gestisce richiesta POST aggiornamento contatto aziendale
	 * @param Id					id contatto aziendale
	 * @param referenceContact		contatto aziendale da modificare
	 * @return						un oggetto contatto aziendale in formato Json
	 */
	@PostMapping(value = "/update/{id}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<ReferenceContact> updateContact(@PathVariable int id, @RequestBody ReferenceContact referenceContact){
		try {
			return new ResponseEntity<>(referServ.updateReferenceContact(id, referenceContact), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_UPDATE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	/**
	 * Gestisce richiesta GET cancellazione contatto aziendale
	 * @param Id	id contatto aziendale
	 * @return		l'oggetto contatto aziendale cancellato in formato Json
	 */
	@GetMapping(value = "/delete/{id}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<ReferenceContact> deleteContact(@PathVariable int id){
		try {
			return new ResponseEntity<>(referServ.deliteReferenceContact(id), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_DELETE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	
	
	
	

}
