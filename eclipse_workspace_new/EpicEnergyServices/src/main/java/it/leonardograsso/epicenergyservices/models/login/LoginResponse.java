
/**
 * Modello di risposta login 
 * 
 * @author LeoGrasso
 */

package it.leonardograsso.epicenergyservices.models.login;


import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponse {
	// Token
	private String token;	
	// Imposta il prefisso che indica il tipo di Token
	private final String type = "Bearer";
	// Dati dell'utente
	private int id;
	private String username;
	private String email;
	private List<String> roles;
	private Date expirationTime;
		
	
}
