
/**
 * Interfaccia di riferimento per tutte le classi
 * che rappresentano i dati aziendali
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.models.customer;


public interface CompanyDataItem extends CorporateItem {

}
