
/**
 * REST Controller 
 * Gestisce le request per la classe Email
 * @author LeoGrasso
 */

package it.leonardograsso.epicenergyservices.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.leonardograsso.epicenergyservices.models.customer.Email;
import it.leonardograsso.epicenergyservices.services.impl.CustomerServiceImpl;
import it.leonardograsso.epicenergyservices.services.impl.EmailServiceImpl;
import it.leonardograsso.epicenergyservices.services.impl.ReferenceContactServiceImpl;

@RestController
@RequestMapping("/customer/email")
public class EmailController {
	
	@Autowired
	private EmailServiceImpl emailSer;
	
	@Autowired
	private CustomerServiceImpl customerSer;
	
	@Autowired
	private ReferenceContactServiceImpl referenceServ;
	
	/*
	 * Classe Message di tipo Utility
	 * Raccoglie i messaggi di errore delle eccezioni
	 * Il costruttore privato mi assicura che questa classe non possa essere istanziata
	 */
	protected static class Messages {
		public static final String ERROR_SAVE = "An unexpected error occurred while saving";
		public static final String ERROR_UPDATE = "An unexpected error occurred while updating";
		public static final String ERROR_DELETE = "An unexpected error occurred while deleting";

		  private Messages() {
			    throw new IllegalStateException("Utility class");
			  }
	}
	
	static Logger log = LoggerFactory.getLogger(EmailController.class);
	
	
	/**
	 * Gestisce richiesta POST inserimento nuovo indirizzo email
	 * @param customerId	id cliente
	 * @param referenceId	id referente aziendale
	 * @param address		riceve un oggetto Address in formato Json
	 * @return				un oggetto Address in formato Json
	 */
	@PostMapping(value = "/add/{customerId}/{referenceId}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<Email> addEmail(@PathVariable Integer customerId, @PathVariable(required = false) Integer referenceId, @RequestBody Email email){
		try {
			/*
			 * Se è presente solo l'id azienda è una mail aziendale
			 * altrimenti sarà la mail di un referente dell'azienda
			 */
			if(referenceId != null) {
				var cust = customerSer.findById(customerId);
				var refer = referenceServ.findById(referenceId);
				email.setCustomer(cust.get());
				email.setReferenceContact(refer.get());
				return new ResponseEntity<>(emailSer.saveEmail(email), HttpStatus.OK);
			} else {
				var cust = customerSer.findById(customerId);
				email.setCustomer(cust.get());
				email.setReferenceContact(null);
				return new ResponseEntity<>(emailSer.saveEmail(email), HttpStatus.OK);
			}
		} catch (Exception e) {
			log.error(Messages.ERROR_SAVE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	/**
	 * Gestisce richiesta POST aggiornamento indirizzo email
	 * @param Id			id indirizzo email
	 * @param email			email da modificare
	 * @return				un oggetto Email in formato Json
	 */
	@PostMapping(value = "/update/{id}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<Email> updateEmail(@PathVariable int id, @RequestBody Email email){
		try {
			return new ResponseEntity<>(emailSer.updateEmail(id, email), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_UPDATE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	/**
	 * Gestisce richiesta GET cancellazione indirizzo email
	 * @param Id	id indirizzo email
	 * @return		l'oggetto Email cancellato in formato Json
	 */
	@GetMapping(value = "/delete/{id}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<Email> deleteEmail(@PathVariable int id){
		try {
			return new ResponseEntity<>(emailSer.deliteEmail(id), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_DELETE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	
	
	

}
