
/**
 * Gestione delle fatture.
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.services;

import java.time.LocalDate;
import java.util.List;

import it.leonardograsso.epicenergyservices.models.invoice.Invoice;

public interface InvoiceService extends BaseService{
	
	/**
	 * salvataggio di una fattura
	 * 
	 * @param customerId	id dell'azienda a cui è riferita la fattura
	 * @param invoice		fattura da salvare nel database
	 * @return 				un oggetto Invoice
	 */
	public Invoice saveInvoice(int customerId, Invoice invoice);
	
	/**
	 * modifica di una fattura
	 * 
	 * @param id 			identificativo fattura
	 * @param invoice 		fattura da modificare
	 * @return un oggetto 	fattura
	 */
	public Invoice updateInvoice (int id, Invoice invoice);
		
	/**
	 * cancellazione di una fattura
	 * 
	 * @param id 			identificativo fattura
	 * @return un oggetto 	fattura
	 */
	public Invoice deleteInvoice(int id);
	
	/**
	 * 
	 * @param customerId id del cliente associato alla fattura
	 * @param pageable implementa la paginazione
	 * @return lista di fatture filtrata per cliente
	 */
	public List<Invoice> findAllByCustomerId(int customerId, Integer page, Integer size, String sort);
	
	/**
	 * 
	 * @param statusId id dello stato fattura
	 * @param pageable implementa la paginazione
	 * @return lista di fatture filtrata per stato
	 */
	public List<Invoice> findAllByStatusId(int statusId, Integer page, Integer size, String sort);
	
	/**
	 * 
	 * @param date data di emissione della fattura
	 * @param pageable implementa la paginazione
	 * @return lista di fatture filtrata per data di emissione
	 */
	public List<Invoice> findAllByDate(LocalDate date, Integer page, Integer size, String sort);
	
	/**
	 * 
	 * @param year anno di emissione ricercato
	 * @param pageable implementa la paginazione
	 * @return lista di fatture filtrata per anno di emissione
	 */
	public List<Invoice> findAllByYear(int year, Integer page, Integer size, String sort);
	
	/**
	 * 
	 * @param min valore minimo dell'intervallo di ricerca
	 * @param max valore massimo dell'intervallo di ricerca
	 * @param pageable implementa la paginazione
	 * @return lista di fatture filtrata per intervallo di importo
	 */
	public List<Invoice> findAllByAmountDueBetween(double min, double max, Integer page, Integer size, String sort);
	
	/**
	 * 
	 * @param id	id fattura
	 * @return		fattura
	 */
	public Invoice findById (int id);

	
	

}
