
/**
 * Classe model dei ruoli 
 * per l'accesso all'applicazione
 * 
 * @author LeoGrasso
 */

package it.leonardograsso.epicenergyservices.models.user;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import it.leonardograsso.epicenergyservices.models.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@EqualsAndHashCode (callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity (name = "roles")
public class Role extends BaseEntity {
	
	/**
	 * Nome del ruolo.
	 */
	@EqualsAndHashCode.Include
	@ToString.Include
	@Column(length = 20, unique = true)
	private String name;
	/**
	 * Utenti nel ruolo.
	 */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "role_id"), inverseJoinColumns = @JoinColumn(name = "user_id"))
	private final Set<User> users = new HashSet<>();

	/**
	 * Costruttore per il builder.
	 */
	@Builder(setterPrefix = "with")
	public Role(int id, String name) {
		super();
		setId(id);
		this.name = name;
	}

}
