
/**
 * Classe che rappresenta un email
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.models.customer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.leonardograsso.epicenergyservices.models.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity (name = "emails")
public class Email extends BaseEntity {
	
	/**
	 * azienda cliente a cui è riferita la mail
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private Customer customer;
	
	/**
	 * mail riferita ad un referente dell'azienda
	 */
	@OneToOne (mappedBy = "email")
	@JsonIgnore
	private ReferenceContact referenceContact;
	
	
	/**
	 * valore della mail, deve essere univoca
	 * il limite tecnologico imposto alla mail è di 320 caratteri
	 */
	@Column(length = 320, unique = true)
	private String emailAddress;
	
	/**
	 * la mail può essere di tre tipi:
	 * --> Business		mail dell'azienda o di un suo referente usata per le normali comunicazioni 
	 * --> PEC			indirizzo di posta certificata per le comunicazioni di tipo contrattuale
	 * --> USER			mail associata ad un utente dell'applicazione, non ad un cliente
	 */
	@Enumerated(EnumType.STRING)
	private EmailType type;
	
		public enum EmailType {
			BUSINESS, PEC, USER
		}

}
