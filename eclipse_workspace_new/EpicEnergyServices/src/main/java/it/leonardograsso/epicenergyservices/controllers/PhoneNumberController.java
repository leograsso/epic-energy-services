
/**
 * REST Controller 
 * Gestisce le request per la classe PhoneNumber
 * @author LeoGrasso
 */

package it.leonardograsso.epicenergyservices.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.leonardograsso.epicenergyservices.models.customer.PhoneNumber;
import it.leonardograsso.epicenergyservices.services.impl.CustomerServiceImpl;
import it.leonardograsso.epicenergyservices.services.impl.PhoneNumberServiceImpl;
import it.leonardograsso.epicenergyservices.services.impl.ReferenceContactServiceImpl;

@RestController
@RequestMapping("/customer/phone")
public class PhoneNumberController {
	
	@Autowired
	private PhoneNumberServiceImpl phoneServ;
	
	@Autowired
	private CustomerServiceImpl customerSer;
	
	@Autowired
	private ReferenceContactServiceImpl referenceServ;
	
	/*
	 * Classe Message di tipo Utility
	 * Raccoglie i messaggi di errore delle eccezioni
	 * Il costruttore privato mi assicura che questa classe non possa essere istanziata
	 */
	protected static class Messages {
		public static final String ERROR_SAVE = "An unexpected error occurred while saving";
		public static final String ERROR_UPDATE = "An unexpected error occurred while updating";
		public static final String ERROR_DELETE = "An unexpected error occurred while deleting";

		  private Messages() {
			    throw new IllegalStateException("Utility class");
			  }
	}
	
	static Logger log = LoggerFactory.getLogger(PhoneNumberController.class);
	
	
	/**
	 * Gestisce richiesta POST inserimento nuovo numero telefonico
	 * @param customerId	id cliente
	 * @param referenceId	id referente aziendale
	 * @param phoneNumber	riceve un oggetto PhoneNumber in formato Json
	 * @return				un oggetto PhoneNumber in formato Json
	 */
	@PostMapping(value = "/add/{customerId}/{referenceId}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<PhoneNumber> addPhoneNumber(@PathVariable Integer customerId, @PathVariable(required = false) Integer referenceId, @RequestBody PhoneNumber phoneNumber){
		try {
			/*
			 * Se è presente solo l'id azienda è un numero aziendale
			 * altrimenti sarà il numero di un referente dell'azienda
			 */
			if(referenceId != null) {
				var cust = customerSer.findById(customerId);
				var refer = referenceServ.findById(referenceId);
				phoneNumber.setCustomer(cust.get());
				phoneNumber.setReferenceContact(refer.get());
				return new ResponseEntity<>(phoneServ.savePhoneNumber(phoneNumber), HttpStatus.OK);
			} else {
				var cust = customerSer.findById(customerId);
				phoneNumber.setCustomer(cust.get());
				phoneNumber.setReferenceContact(null);
				return new ResponseEntity<>(phoneServ.savePhoneNumber(phoneNumber), HttpStatus.OK);
			}
		} catch (Exception e) {
			log.error(Messages.ERROR_SAVE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	/**
	 * Gestisce richiesta POST aggiornamento numero telefonico
	 * @param Id			id numero telefonico
	 * @param email			numero telefonico da modificare
	 * @return				un oggetto numero telefonico in formato Json
	 */
	@PostMapping(value = "/update/{id}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<PhoneNumber> updatePhoneNumber(@PathVariable int id, @RequestBody PhoneNumber phoneNumber){
		try {
			return new ResponseEntity<>(phoneServ.updatePhoneNumber(id, phoneNumber), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_UPDATE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	/**
	 * Gestisce richiesta GET cancellazione numero telefonico
	 * @param Id	id numero telefonico
	 * @return		l'oggetto PhoneNumber cancellato in formato Json
	 */
	@GetMapping(value = "/delete/{id}")
	@PreAuthorize("hasRole('admin')")
	public ResponseEntity<PhoneNumber> deletePhoneNumber(@PathVariable int id){
		try {
			return new ResponseEntity<>(phoneServ.delitePhoneNumber(id), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.ERROR_DELETE, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}

}
