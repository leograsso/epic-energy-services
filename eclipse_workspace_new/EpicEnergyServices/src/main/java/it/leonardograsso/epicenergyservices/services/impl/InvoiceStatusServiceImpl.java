package it.leonardograsso.epicenergyservices.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.leonardograsso.epicenergyservices.models.invoice.InvoiceStatus;
import it.leonardograsso.epicenergyservices.repositories.InvoiceStatusRepository;
import it.leonardograsso.epicenergyservices.services.InvoiceStatusService;
import it.leonardograsso.epicenergyservices.services.exceptions.EntityNotFoundException;
import it.leonardograsso.epicenergyservices.services.exceptions.ServiceException;

@Service
public class InvoiceStatusServiceImpl implements InvoiceStatusService {
	
	Logger log = LoggerFactory.getLogger(InvoiceStatusServiceImpl.class);

	@Autowired
	private InvoiceStatusRepository invoiceStatus;

	@Override
	public InvoiceStatus addStatus(InvoiceStatus status) {
		try {
			//se questo stato non è presente lo aggiunge
			if(invoiceStatus.findInvoiceStatusByStatusNameIgnoreCase(status.getStatusName()).isEmpty()) {
				InvoiceStatus is = new InvoiceStatus();
				is.setStatusName(status.getStatusName());
				return invoiceStatus.save(is);
			} else {
				//se lo stato è presente non lo aggiunge
				log.error("already existing invoice status type -> {}", status.getStatusName());
				return status;
			}	
		} catch (Exception e) {
			log.error("Unable to save the Invoice Status {}", status.getStatusName());
			throw new ServiceException(e);
		}
	}

	
	@Override
	public InvoiceStatus remuveStatus(int id) {
		try {
			//se lo stato è presente lo cancella
			if(invoiceStatus.findById(id).isPresent()) {
				invoiceStatus.deleteById(id);
				return invoiceStatus.findById(id).get();
			} else {
				log.error("Unable to find the Invoice Status whith id {}", id);
				return null;
			}	
		} catch (Exception e) {
			log.error("Unable to delete the Invoice Status whith id {}", id);
			throw new ServiceException(e);
		}
	}


	@Override
	public InvoiceStatus getById(int id) {
		try {
			return invoiceStatus.findById(id).get();
		} catch (Exception e) {
			log.error("Unable to find invoice status with id: {}", id);
			throw new EntityNotFoundException(id, InvoiceStatus.class);
		}
	}
	
	

	

}
