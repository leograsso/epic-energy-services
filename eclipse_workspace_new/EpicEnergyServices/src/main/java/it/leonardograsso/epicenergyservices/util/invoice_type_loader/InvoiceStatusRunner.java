
/**
 * Classe runner utility
 * serve a caricare i tipi di stato fattura
 * 
 * @author LeoGrasso
 */

package it.leonardograsso.epicenergyservices.util.invoice_type_loader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import it.leonardograsso.epicenergyservices.models.invoice.decor_status.PaidStatus;
import it.leonardograsso.epicenergyservices.models.invoice.decor_status.UnsolvedStatus;
import it.leonardograsso.epicenergyservices.services.impl.InvoiceStatusServiceImpl;

@Component
@Order(value = 1)
public class InvoiceStatusRunner implements CommandLineRunner {

	@Autowired
	InvoiceStatusServiceImpl invoiceStatus;
	
	@Autowired
	PaidStatus paid;
	
	@Autowired
	UnsolvedStatus unpaid;
	
	
	
	@Override
	public void run(String... args) throws Exception {
		
		/**
		 * di seguito sono richiamati i metodi per
		 * aggiungere nuovi stati fattura
		 */
	
		//carica stato pagato
		//invoiceStatus.addStatus(paid);
		//carica stato insoluto
		//invoiceStatus.addStatus(unpaid);
		
		
		
		
		
		
	}

}
