
/**
 * Indirizzo del cliente
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.models.customer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIncludeProperties;

import it.leonardograsso.epicenergyservices.models.BaseEntity;
import it.leonardograsso.epicenergyservices.util.cityloader.CityModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity (name = "addresses")
public class Address extends BaseEntity{
	
	/**
	 * cliente a cui è associato l'indirizzo
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private Customer customer;
	
	/**
	 * via
	 */
	private String street;
	
	/**
	 * numero civico
	 */
	@Column(length = 5)
	private int number;
	
	/**
	 * località/contrada/quartiere
	 */
	private String fraction;
	
	/**
	 * codice avviamento postale CAP
	 */
	@Column(length = 7)
	private int postalCode;
	
	/**
	 * tipo di indirizzo: sede legale o operativa
	 */
	@Column(length = 20, nullable = false)
	@Enumerated(EnumType.STRING)
	private AddressType type;
	
		public enum AddressType {
			REGISTERED_OFFICE, OPERATIVE_OFFICE
		}
	
	/**
	 * città a cui è associato l'indirizzo
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIncludeProperties(value = { "name", "acronym" })
	private CityModel city;
	
	
	

}
