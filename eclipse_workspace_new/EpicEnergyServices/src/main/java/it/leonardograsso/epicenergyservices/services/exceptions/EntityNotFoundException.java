package it.leonardograsso.epicenergyservices.services.exceptions;

import lombok.Getter;

/**
 * Eccezione che comunica l'impossibilità nel recuperare un'entità.
 * 
 * @author LeoGrasso
 *
 */
public class EntityNotFoundException extends ServiceException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Chiave sulla quale è stata effettuata la ricerca.
	 */
	@Getter
	private final String key;
	
	/**
	 * Tipo dell'entità che veniva ricercata.
	 */
	@Getter
	private final Class<?> type;

	public EntityNotFoundException(String key, Class<?> type) {
		super();
		this.key = key;
		this.type = type;
	}

	public EntityNotFoundException(Integer key, Class<?> type) {
		this(key.toString(), type);
	}
}