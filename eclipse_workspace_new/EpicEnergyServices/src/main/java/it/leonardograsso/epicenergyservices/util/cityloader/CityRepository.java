
/**
 * Repository dell'entity City
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.util.cityloader;


import java.util.Optional;

import org.springframework.data.repository.CrudRepository;


public interface CityRepository extends CrudRepository <CityModel, Integer>{
	
	/**
	 * 
	 * @param name	nome della città
	 * @return		restituisce una città
	 */
	public Optional<CityModel> findCityModelByNameIgnoreCase(String name); 
	


}
