
/**
 * Gestione degli indirizzi.
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.services;

import it.leonardograsso.epicenergyservices.models.customer.Address;


public interface AddressService extends BaseService{

	/**
	 * salva un nuovo indirizzo
	 * 
	 * @param address	indirizzo
	 */
	Address saveAddress(Address address);

	/**
	 * modifica un indirizzo
	 * 
	 * @param id			id dell'indirizzo
	 * @param address 		indirizzo
	 * @return				indirizzo 
	 */
	Address updateAddress(int id, Address address);

	
	/**
	 * cancella indirizzo
	 * 
	 * @param id
	 * @return indirizzo cancellato
	 */
	Address deliteAddress(int id);
	
}
