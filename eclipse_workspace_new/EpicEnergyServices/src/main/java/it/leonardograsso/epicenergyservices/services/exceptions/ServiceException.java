package it.leonardograsso.epicenergyservices.services.exceptions;

/**
 * Base per le eccezioni delle classi service.
 * 
 * @author LeoGrasso
 *
 */

public class ServiceException extends AppException {

	private static final long serialVersionUID = 1L;	
	
	
	public ServiceException() {
		super();
	}

	public ServiceException(Throwable cause) {
		super(cause);
	}
	

}
