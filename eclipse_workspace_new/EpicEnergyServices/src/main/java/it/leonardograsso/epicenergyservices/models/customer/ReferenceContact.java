
/**
 * Classe che rappresenta un referente aziendale
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.models.customer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.leonardograsso.epicenergyservices.models.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity (name = "reference_contacts")
public class ReferenceContact extends BaseEntity {
	
	/**
	 * azienda cliente
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private Customer customer;
	
	/**
	 * nome referente
	 */
	@Column(length = 50)
	private String name;
	
	/**
	 * cognome referente
	 */
	@Column(length = 50)
	private String surname;
	
	/**
	 * mail referente
	 */
	@OneToOne
	private Email email;
	
	/**
	 * telefono referente
	 */
	@OneToOne
	private PhoneNumber phoneNumber;
	
	/**
	 * ruolo ricoperto nell'azienda
	 */
	@Column(length = 30)
	private String companyRole;

}
