
/**
 * RESTcontroller dell'entity City
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.util.cityloader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cities")
public class CityController {
	
	/*
	 * Classe di tipo Utility
	 * Raccoglie i messaggi di errore delle eccezioni
	 * Il costruttore privato mi assicura che questa classe non possa essere istanziata
	 */
	protected static class Messages {
		public static final String FILE_EXCEPTION = "An error occurred in the handling of the cities.csv file";

		  private Messages() {
			    throw new IllegalStateException("Utility class");
			  }
	}
	
	static Logger log = LoggerFactory.getLogger(CityController.class);
	
	@Autowired
	private CityServiceImpl city;
	
	/*
	 * Richiama il metodo cityLoader() della classe service
	 * Carica le città italiane dal file csv alla tabella city del database.
	 * La scrittura avviene solo se la tabella city è vuota altrimenti restituisce un messaggio
	 * di errore
	 */
	@GetMapping("/upload")
	public ResponseEntity<String> uploadCities() {
		try {
			return new ResponseEntity<>(city.cityLoader(), HttpStatus.OK);
		} catch (Exception e) {
			log.error(Messages.FILE_EXCEPTION, e);
			return new ResponseEntity<>
				(Messages.FILE_EXCEPTION, HttpStatus.BAD_REQUEST);
		}
	}

}
