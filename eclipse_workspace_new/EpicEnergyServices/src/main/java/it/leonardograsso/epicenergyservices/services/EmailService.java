
/**
 * Gestione delle mail.
 * 
 * @author LeoGrasso
 *
 */

package it.leonardograsso.epicenergyservices.services;

import java.util.Optional;

import it.leonardograsso.epicenergyservices.models.customer.Email;



public interface EmailService extends BaseService{
	
	/**
	 * salva un nuovo indirizzo email
	 * 
	 * @param email	 indirizzo email
	 */
	Email saveEmail(Email email);

	
	/**
	 * modifica un indirizzo email
	 * 
	 * @param id			id dell'indirizzo email
	 * @param email			indirizzo email
	 * @return				indirizzo email
	 */
	Email updateEmail(int id, Email email);

	
	/**
	 * cancella indirizzo email
	 * 
	 * @param id
	 * @return indirizzo email cancellato
	 */
	Email deliteEmail(int id);
	
	
	/**
	 * ricerca email per id
	 * 
	 * @param id	id email
	 * @return		cliente
	 */
	public Optional<Email> findById(int id);

}
